package com.dhr.smilesoft;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmilesoftApplication {

    public static void main(String[] args) {
        SpringApplication.run(SmilesoftApplication.class);
    }
}
