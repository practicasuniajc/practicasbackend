package com.dhr.smilesoft.controller;

import com.dhr.smilesoft.model.AnaDinaOclusion;
import com.dhr.smilesoft.model.request.AnaDinaOclusionRequest;
import com.dhr.smilesoft.service.AnaDinaOclusionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/anadinaoclusion")
@Api(tags = "ana_dinaoclusion")
public class AnaDianaOclusionController {

    private static final  ModelMapper modelMapper = new ModelMapper();

    @Autowired
    private AnaDinaOclusionService anaDinaOclusionService;

    @PostMapping(path = "/insert")
    @ApiOperation(value = "Insert AnaDinaOclusion", response = AnaDinaOclusion.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public  AnaDinaOclusion insertAnaDinaOclusion(@RequestBody AnaDinaOclusionRequest anaDinaOclusionRequest){
        AnaDinaOclusion anaDinaOclusion = modelMapper.map(anaDinaOclusionRequest, AnaDinaOclusion.class);
        return anaDinaOclusionService.saveAnaDinaOclusion(anaDinaOclusion);
    }

    @PostMapping(path = "/Update")
    @ApiOperation(value = "Update AnaDinaOclusion", response = AnaDinaOclusion.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public  AnaDinaOclusion updateAnaDinaOclusion(@RequestBody AnaDinaOclusionRequest anaDinaOclusionRequest){
        AnaDinaOclusion anaDinaOclusion = modelMapper.map(anaDinaOclusionRequest, AnaDinaOclusion.class);
        return anaDinaOclusionService.updateAnaDinaOclusion(anaDinaOclusion);
    }

    @GetMapping(path = "/all")
    @ApiOperation(value = "Get AnaDinaOclusion all", response = AnaDinaOclusion.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public List<AnaDinaOclusion> getAllAnaDinaOclusion (){
        return  anaDinaOclusionService.findAll();
    }

    @DeleteMapping(path = "/delete/id")
    @ApiOperation(value = "Delete AnaDinaOclusion", response = AnaDinaOclusion.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public  void  deleteAnaDinaOclusionbyID(@RequestParam(name = "id")Long id ){
        anaDinaOclusionService.delectAbaDianaOclusionByID(id);
    }

}
