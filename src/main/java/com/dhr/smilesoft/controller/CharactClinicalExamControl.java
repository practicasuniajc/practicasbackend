package com.dhr.smilesoft.controller;

import com.dhr.smilesoft.model.CharactClinicalExam;
import com.dhr.smilesoft.model.request.CharactClinicalExamRequest;
import com.dhr.smilesoft.service.CharactClinicalExamService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/clinicalExam")
@Api(tags = "clinica exam")
public class CharactClinicalExamControl {

    private static final ModelMapper modelMapper = new ModelMapper();

    @Autowired
    private CharactClinicalExamService charactClinicalExamService;

    @PostMapping(path = "/insert")
    @ApiOperation(value = "Insert exam", response = CharactClinicalExam.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public CharactClinicalExam insert(@RequestBody CharactClinicalExamRequest charactClinicalExamRequest) {
        CharactClinicalExam charactClinicalExam = modelMapper.map(charactClinicalExamRequest, CharactClinicalExam.class);
        return charactClinicalExamService.saveClinicalExam(charactClinicalExam); }

    @PostMapping(path = "/update")
    @ApiOperation(value = "Update exam", response = CharactClinicalExam.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public CharactClinicalExam Update(@RequestBody CharactClinicalExamRequest charactClinicalExamRequest) {
        CharactClinicalExam charactClinicalExam = modelMapper.map(charactClinicalExamRequest, CharactClinicalExam.class);
        return charactClinicalExamService.updateClinicalExam(charactClinicalExam); }

    @GetMapping(path = "/all")
    @ApiOperation(value = "Get All Table", response = CharactClinicalExam.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public List<CharactClinicalExam> getAllExam() { return charactClinicalExamService.findAll(); }

    @DeleteMapping(path = "/delete/id")
    @ApiOperation(value = "Delete periodontics", response = CharactClinicalExam.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public  void  deletebyID(@RequestParam(name = "id")Long id ){ charactClinicalExamService.deleteByID(id); }




}
