package com.dhr.smilesoft.controller;

import com.dhr.smilesoft.model.GingivalArchitecture;
import com.dhr.smilesoft.model.request.GingivalArchitectureRequest;
import com.dhr.smilesoft.service.GingivalArchitectureService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/gingivalArch")
@Api(tags = "gingival architecture")
public class GingivalArchitectureController {

    private static final ModelMapper modelMapper = new ModelMapper();

    @Autowired
    private GingivalArchitectureService gingivalArchitectureService;

    @PostMapping(path = "/insert")
    @ApiOperation(value = "Insert gingival arch", response = GingivalArchitecture.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public GingivalArchitecture insert(@RequestBody GingivalArchitectureRequest gingivalArchitectureRequest) {
        GingivalArchitecture gingivalArchitecture = modelMapper.map(gingivalArchitectureRequest, GingivalArchitecture.class);
        return gingivalArchitectureService.saveArc(gingivalArchitecture); }

    @PostMapping(path = "/update")
    @ApiOperation(value = "Insert gingival arch", response = GingivalArchitecture.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public GingivalArchitecture update(@RequestBody GingivalArchitectureRequest gingivalArchitectureRequest) {
        GingivalArchitecture gingivalArchitecture = modelMapper.map(gingivalArchitectureRequest, GingivalArchitecture.class);
        return gingivalArchitectureService.updateArc(gingivalArchitecture); }

    @GetMapping(path = "/all")
    @ApiOperation(value = "Get All Table", response = GingivalArchitecture.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public List<GingivalArchitecture> getAllGingivalArch() { return gingivalArchitectureService.findAll(); }

    @DeleteMapping(path = "/delete/id")
    @ApiOperation(value = "Delete periodontics", response = GingivalArchitecture.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public  void  deletebyID(@RequestParam(name = "id")Long id ){
        gingivalArchitectureService.deletebyID(id);
    }
}
