package com.dhr.smilesoft.controller;

import com.dhr.smilesoft.model.GingivalDescription;
import com.dhr.smilesoft.model.request.GingivalDescriptionRequest;
import com.dhr.smilesoft.service.GingivalDescriptionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/gingivalDesc")
@Api(tags = "gingival decription")
public class GingivalDescriptionController {

    private static final ModelMapper modelMapper = new ModelMapper();

    @Autowired
    private GingivalDescriptionService gingivalDescriptionService;

    @PostMapping(path = "/insert")
    @ApiOperation(value = "Insert gingival desc", response = GingivalDescription.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public GingivalDescription insert(@RequestBody GingivalDescriptionRequest gingivalDescriptionRequest) {
        GingivalDescription gingivalDescription = modelMapper.map(gingivalDescriptionRequest, GingivalDescription.class);
        return gingivalDescriptionService.saveGingivalDescription(gingivalDescription); }

    @PutMapping(path = "/update")
    @ApiOperation(value = "Update ", response = GingivalDescription.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public GingivalDescription update(@RequestBody GingivalDescriptionRequest gingivalDescriptionRequest) {
        GingivalDescription gingivalDescription = modelMapper.map(gingivalDescriptionRequest, GingivalDescription.class);
        return gingivalDescriptionService.saveGingivalDescription(gingivalDescription); }

    @GetMapping(path = "/all")
    @ApiOperation(value = "Get All Table", response = GingivalDescription.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public List<GingivalDescription> getAllPeriodontics() { return gingivalDescriptionService.findAll(); }

    @DeleteMapping(path = "/delete/id")
    @ApiOperation(value = "Delete periodontics", response = GingivalDescription.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public  void  deletebyID(@RequestParam(name = "id")Long id ){
        gingivalDescriptionService.deletebyID(id);
    }



}
