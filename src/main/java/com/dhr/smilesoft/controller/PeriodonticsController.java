package com.dhr.smilesoft.controller;

import com.dhr.smilesoft.model.Periodontics;
import com.dhr.smilesoft.model.request.PeriodonticsRequest;
import com.dhr.smilesoft.service.PeriodonticsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/periodontics")
@Api(tags = "periodontics")
public class PeriodonticsController {

    private static final ModelMapper modelMapper = new ModelMapper();

    @Autowired
    private PeriodonticsService periodonticsService;

    @PostMapping(path = "/insert")
    @ApiOperation(value = "Insert Periodontics", response = Periodontics.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public Periodontics insertPeriodontics(@RequestBody PeriodonticsRequest periodonticsRequest) {
        Periodontics periodontics = modelMapper.map(periodonticsRequest, Periodontics.class);
        return periodonticsService.savePeriodontics(periodontics); }

    @PutMapping(path = "/update")
    @ApiOperation(value = "Update Table", response = Periodontics.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public Periodontics updatePeriodontics(@RequestBody PeriodonticsRequest periodonticsRequest) {
        Periodontics periodontics = modelMapper.map(periodonticsRequest, Periodontics.class);
        return periodonticsService.updatePeriodontics(periodontics); }

    @GetMapping(path = "/all")
    @ApiOperation(value = "Get All Table", response = Periodontics.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public List<Periodontics> getAllPeriodontics() { return periodonticsService.findAll(); }

    @DeleteMapping(path = "/delete/id")
    @ApiOperation(value = "Delete periodontics", response = Periodontics.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public  void  deletePeriodonticsByID(@RequestParam(name = "id")Long id ){
        periodonticsService.deletePeriodonticsbyID(id);
    }

}
