package com.dhr.smilesoft.controller;

import com.dhr.smilesoft.model.RxAnatomicalStructure;
import com.dhr.smilesoft.model.request.RxAnatomicalStructureRequest;
import com.dhr.smilesoft.service.RxAnatomicalStructureService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rxanatomical")
@Api(tags = "rx_anatomical")
public class RxAnatomicalStructureController {

    private static final ModelMapper modelMapper = new ModelMapper();

    @Autowired
    private RxAnatomicalStructureService rxAnatomicalStructureService;


    @PostMapping(path = "/insert")
    @ApiOperation(value = "Insert RxAnatomicalStructure", response = RxAnatomicalStructure.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public RxAnatomicalStructure insertRxAnatomicalStructure(@RequestBody RxAnatomicalStructureRequest rxAnatomicalStructureRequest) {
        RxAnatomicalStructure rxAnatomicalStructure = modelMapper.map(rxAnatomicalStructureRequest, RxAnatomicalStructure.class);
        return rxAnatomicalStructureService.saveRxAnatomicalStructure(rxAnatomicalStructure);
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "Update RxAnatomicalStructure", response = RxAnatomicalStructure.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public RxAnatomicalStructure updateRxAnatomicalStructure(@RequestBody RxAnatomicalStructureRequest rxAnatomicalStructureRequest) {
        RxAnatomicalStructure rxAnatomicalStructure = modelMapper.map(rxAnatomicalStructureRequest, RxAnatomicalStructure.class);
        return rxAnatomicalStructureService.updateRxAnatomicalStructure(rxAnatomicalStructure);
    }

    @GetMapping(path = "/all")
    @ApiOperation(value = "Get RxAnatomicalStructure all", response = RxAnatomicalStructure.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public List<RxAnatomicalStructure> getAllRxAnatomicalStructure (){ return  rxAnatomicalStructureService.findAll(); }


    @DeleteMapping(path = "/delete/id")
    @ApiOperation(value = "Delete RxAnatomicalStructure", response = RxAnatomicalStructure.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public  void  deleteRxAnatomicalStructurebyID(@RequestParam(name = "id")Long id ){
        rxAnatomicalStructureService.delectRxAnatomicalStructureByID(id); }
}

