package com.dhr.smilesoft.controller;
import com.dhr.smilesoft.model.RxPanoramica;
import com.dhr.smilesoft.model.request.RxPanoramicaRequest;
import com.dhr.smilesoft.service.RxPanoramicaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/rxpanoramica")
@Api(tags = "rx_panoramica")
public class RxPanoramicaController {
    private static final ModelMapper modelMapper = new ModelMapper();

    @Autowired
    private RxPanoramicaService rxPanoramicaService;
    @PostMapping(path = "/insert")
    @ApiOperation(value = "Insert RxPanoramica", response = RxPanoramica.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public  RxPanoramica insertRxPanoramica(@RequestBody RxPanoramicaRequest rxPanoramicaRequest){
        RxPanoramica rxPanoramica = modelMapper.map(rxPanoramicaRequest, RxPanoramica.class);
        return rxPanoramicaService.saveRxPanoramica(rxPanoramica);
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "Update RxPanoramica", response = RxPanoramica.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public RxPanoramica updateRxPanoramica (@RequestBody RxPanoramicaRequest rxPanoramicaRequest){
        RxPanoramica rxPanoramica = modelMapper.map(rxPanoramicaRequest, RxPanoramica.class);
        return  rxPanoramicaService.updateRxPanoramica(rxPanoramica);


    }

    @GetMapping(path = "/all")
    @ApiOperation(value = "Get RxPanoramica all", response = RxPanoramica.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public List<RxPanoramica> getAllRxPanoramica (){ return  rxPanoramicaService.findAll(); }


    @DeleteMapping(path = "/delete/id")
    @ApiOperation(value = "Delete RxPanoramica", response = RxPanoramica.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public  void  deleteRxPanoramicabyID(@RequestParam(name = "id")Long id ){
        rxPanoramicaService.delectRxPanoramicaByID(id); }
}
