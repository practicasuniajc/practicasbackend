package com.dhr.smilesoft.controller;

import com.dhr.smilesoft.model.RxPeriapical;
import com.dhr.smilesoft.model.request.RxPeriapicalRequest;
import com.dhr.smilesoft.service.RxPeriapicalService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rxperiapical")
@Api(tags = "rx_periapical")
public class RxPeriapicalController {

    private static final ModelMapper modelMapper = new ModelMapper();

    @Autowired
    private RxPeriapicalService rxPeriapicalService;

    @PostMapping(path = "/insert")
    @ApiOperation(value = "Insert RxPeriapical", response = RxPeriapical.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public  RxPeriapical insertRxPeriapical(@RequestBody RxPeriapicalRequest rxPeriapicalRequest){
        RxPeriapical rxPeriapical = modelMapper.map(rxPeriapicalRequest, RxPeriapical.class);
        return rxPeriapicalService.saveRxPeriapical(rxPeriapical); }

    @PutMapping(path = "/update")
    @ApiOperation(value = "Update RxPeriapical", response = RxPeriapical.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public RxPeriapical updateRxPeriapical (@RequestBody RxPeriapicalRequest rxPeriapicalRequest){
        RxPeriapical rxPeriapical = modelMapper.map(rxPeriapicalRequest, RxPeriapical.class);
        return  rxPeriapicalService.UpdateRxPeriapical(rxPeriapical); }

    @GetMapping(path = "/all")
    @ApiOperation(value = "Get RxPeriapical all", response = RxPeriapical.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public List<RxPeriapical> getAllRxperiapical (){
        return  rxPeriapicalService.findAll();
    }

    @DeleteMapping(path = "/delete/id")
    @ApiOperation(value = "Delete RxPericapical", response = RxPeriapical.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public  void  deleteRxPeriapicalbyID(@RequestParam(name = "id")Long id ){
        rxPeriapicalService.delectRxperiapicalByID(id);
    }

}
