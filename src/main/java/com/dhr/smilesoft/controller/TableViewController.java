package com.dhr.smilesoft.controller;

import com.dhr.smilesoft.model.TableView;
import com.dhr.smilesoft.model.request.TableViewRequest;
import com.dhr.smilesoft.service.TableViewService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@RestController
@RequestMapping("/table")
@Api(tags = "Table")
public class TableViewController {

    private static final ModelMapper modelMapper = new ModelMapper();

    @Autowired
    private TableViewService tableViewService;

    @PostMapping(path = "/insert")
    @ApiOperation(value = "Insert Table", response = TableView.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public TableView insertTable(@RequestBody TableViewRequest tableViewRequest) {
        TableView tableView = modelMapper.map(tableViewRequest, TableView.class);
        return tableViewService.saveTableView(tableView);
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "Update Table", response = TableView.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public TableView updateTable(@RequestBody TableViewRequest tableViewRequest) {
        TableView tableView = modelMapper.map(tableViewRequest, TableView.class);
        return tableViewService.updateTableView(tableView);
    }

    @GetMapping(path = "/all")
    @ApiOperation(value = "Get All Table", response = TableView.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public List<TableView> getAllTable() {
        return tableViewService.findAll();
    }

    @GetMapping(path = "/find/status")
    @ApiOperation(value = "Get Table by Status", response = TableView.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public List<TableView> getTableByStatus(@RequestParam(name = "status") boolean status) {
        return tableViewService.findByStatus(status);
    }

    @DeleteMapping(path = "/delete/id")
    @ApiOperation(value = "Delete Table By Id", response = TableView.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public void deletePatientProfessionalById(@RequestParam(name = "id") Long id){
        tableViewService.deleteTableById(id);
    }

}
