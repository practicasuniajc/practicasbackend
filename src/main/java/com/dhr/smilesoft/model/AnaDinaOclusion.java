package com.dhr.smilesoft.model;


import javax.persistence.*;

@Entity
@Table (name = "ana_dina_oclusion")
public class AnaDinaOclusion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "ado_id")
    private Long adoId;
    @Column(name = "ado_deflexion")
    private boolean deflection;
    @Column(name = "ado_orient_deflex")
    private String deflectOrientation;
    @Column(name = "ado_magnit_deflex")
    private String deflectMagnit;
    @Column(name = "ado_contact_deflect")
    private String deflectContact;
    @Column(name = "ado_excur_protusiva")
    private String protrusExcursion;
    @Column(name = "ado_fchristensen")
    private String fchristensen;
    @Column(name = "ado_excur_latder")
    private String rightSideExcur;
    @Column(name = "ado_protecan_der")
    private String rightProtecan;
    @Column(name = "ado_cont_notrabajo_der")
    private String rightNotWorkCont;
    @Column(name = "ado_excur_latizq")
    private String leftSideExcur;
    @Column(name = "ado_protecan_izq")
    private String leftProtecan;
    @Column(name = "ado_cont_notrabajo_izq")
    private String lefttNotWorkCont;
    @Column(name = "ado_observaciones")
    private String observations;
    @Column(name = "ado_fecha")
    private String date;
    @OneToOne
    @JoinColumn ( name = "fk_detalle_hc_id", referencedColumnName = "cons_id")
    private DetailHC detailHC;

    public AnaDinaOclusion() {
        //constructor
    }

    public Long getAdoId() {
        return adoId;
    }

    public void setAdoId(Long adoId) {
        this.adoId = adoId;
    }

    public boolean isDeflection() {
        return deflection;
    }

    public void setDeflection(boolean deflection) {
        this.deflection = deflection;
    }

    public String getDeflectOrientation() {
        return deflectOrientation;
    }

    public void setDeflectOrientation(String deflectOrientation) {
        this.deflectOrientation = deflectOrientation;
    }

    public String getDeflectMagnit() {
        return deflectMagnit;
    }

    public void setDeflectMagnit(String deflectMagnit) {
        this.deflectMagnit = deflectMagnit;
    }

    public String getDeflectContact() {
        return deflectContact;
    }

    public void setDeflectContact(String deflectContact) {
        this.deflectContact = deflectContact;
    }

    public String getProtrusExcursion() {
        return protrusExcursion;
    }

    public void setProtrusExcursion(String protrusExcursion) {
        this.protrusExcursion = protrusExcursion;
    }

    public String getFchristensen() {
        return fchristensen;
    }

    public void setFchristensen(String fchristensen) {
        this.fchristensen = fchristensen;
    }

    public String getRightSideExcur() {
        return rightSideExcur;
    }

    public void setRightSideExcur(String rightSideExcur) {
        this.rightSideExcur = rightSideExcur;
    }

    public String getRightProtecan() {
        return rightProtecan;
    }

    public void setRightProtecan(String rightProtecan) {
        this.rightProtecan = rightProtecan;
    }

    public String getRightNotWorkCont() {
        return rightNotWorkCont;
    }

    public void setRightNotWorkCont(String rightNotWorkCont) {
        this.rightNotWorkCont = rightNotWorkCont;
    }

    public String getLeftSideExcur() {
        return leftSideExcur;
    }

    public void setLeftSideExcur(String leftSideExcur) {
        this.leftSideExcur = leftSideExcur;
    }

    public String getLeftProtecan() {
        return leftProtecan;
    }

    public void setLeftProtecan(String leftProtecan) {
        this.leftProtecan = leftProtecan;
    }

    public String getLefttNotWorkCont() {
        return lefttNotWorkCont;
    }

    public void setLefttNotWorkCont(String lefttNotWorkCont) {
        this.lefttNotWorkCont = lefttNotWorkCont;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public DetailHC getDetailHC() {
        return detailHC;
    }

    public void setDetailHC(DetailHC detailHC) {
        this.detailHC = detailHC;
    }
}
