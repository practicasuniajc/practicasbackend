package com.dhr.smilesoft.model;

import javax.persistence.*;

@Entity
@Table(name = "caract_examen_clinico")
public class CharactClinicalExam {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "exc_id")
    private Long ceId;
    @Column(name = "exc_sang_generalizado")
    boolean generalBleending;
    @Column(name = "exc_sang_localizado")
    boolean localBleending;
    @Column(name = "exc_sang_estimulo")
    boolean stimulusBleending;
    @Column(name = "exc_sang_espontaneo")
    boolean spontaneousBleending;
    @Column(name = "exc_sang_desc")
    String descriptBleending;
    @Column(name = "exc_exudado")
    boolean exudate;
    @Column(name = "exc_exudado_desc")
    private String descriptExudate;
    @Column(name = "exc_movilidad")
    private boolean mobility;
    @Column(name = "exc_movilidad_desc")
    private String descriptMobility;
    @Column(name = "exc_movilidad_grado")
    private String gradeMobility;
    @Column(name = "exc_lesion_furca")
    private int lesionFurca;
    @Column(name = "exc_furca_desc")
    private String descripFurca;
    @Column(name = "xc_diente_bolsa")
    private boolean toothBag;
    @Column(name = "exc_dientebosa_desc")
    private String descriptToothBag;
    @Column(name = "exc_calculo")
    private boolean calculation;
    @Column(name = "exc_tipo_calculo")
    private String typeCalculation;
    @Column(name = "exc_calculo_desc")
    private String descriptCalculation;
    @OneToOne
    @JoinColumn(name = "fk_periodon_id" , referencedColumnName = "per_id")
    private Periodontics periodontics;

    public CharactClinicalExam() {
        // Costructor
    }

    public Long getCeId() { return ceId; }

    public void setCeId(Long ceId) { this.ceId = ceId; }

    public boolean isGeneralBleending() {
        return generalBleending;
    }

    public void setGeneralBleending(boolean generalBleending) {
        this.generalBleending = generalBleending;
    }

    public boolean isLocalBleending() {
        return localBleending;
    }

    public void setLocalBleending(boolean localBleending) {
        this.localBleending = localBleending;
    }

    public boolean isStimulusBleending() {
        return stimulusBleending;
    }

    public void setStimulusBleending(boolean stimulusBleending) {
        this.stimulusBleending = stimulusBleending;
    }

    public boolean isSpontaneousBleending() {
        return spontaneousBleending;
    }

    public void setSpontaneousBleending(boolean spontaneousBleending) { this.spontaneousBleending = spontaneousBleending; }

    public String getDescriptBleending() {
        return descriptBleending;
    }

    public void setDescriptBleending(String descriptBleending) {
        this.descriptBleending = descriptBleending;
    }

    public boolean isExudate() {
        return exudate;
    }

    public void setExudate(boolean exudate) {
        this.exudate = exudate;
    }

    public String getDescriptExudate() {
        return descriptExudate;
    }

    public void setDescriptExudate(String descriptExudate) {
        this.descriptExudate = descriptExudate;
    }

    public boolean isMobility() {
        return mobility;
    }

    public void setMobility(boolean mobility) {
        this.mobility = mobility;
    }

    public String getDescriptMobility() {
        return descriptMobility;
    }

    public void setDescriptMobility(String descriptMobility) {
        this.descriptMobility = descriptMobility;
    }

    public String getGradeMobility() {
        return gradeMobility;
    }

    public void setGradeMobility(String gradeMobility) {
        this.gradeMobility = gradeMobility;
    }

    public int getLesionFurca() {
        return lesionFurca;
    }

    public void setLesionFurca(int lesionFurca) {
        this.lesionFurca = lesionFurca;
    }

    public String getDescripFurca() {
        return descripFurca;
    }

    public void setDescripFurca(String descripFurca) {
        this.descripFurca = descripFurca;
    }

    public boolean isToothBag() {
        return toothBag;
    }

    public void setToothBag(boolean toothBag) {
        this.toothBag = toothBag;
    }

    public String getDescriptToothBag() {
        return descriptToothBag;
    }

    public void setDescriptToothBag(String descriptToothBag) {
        this.descriptToothBag = descriptToothBag;
    }

    public boolean isCalculation() {
        return calculation;
    }

    public void setCalculation(boolean calculation) {
        this.calculation = calculation;
    }

    public String getTypeCalculation() {
        return typeCalculation;
    }

    public void setTypeCalculation(String typeCalculation) {
        this.typeCalculation = typeCalculation;
    }

    public String getDescriptCalculation() {
        return descriptCalculation;
    }

    public void setDescriptCalculation(String descriptCalculation) {
        this.descriptCalculation = descriptCalculation;
    }

    public Periodontics getPeriodontics() {
        return periodontics;
    }

    public void setPeriodontics(Periodontics periodontics) {
        this.periodontics = periodontics;
    }
}
