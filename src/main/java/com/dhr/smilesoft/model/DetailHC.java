package com.dhr.smilesoft.model;



import javax.persistence.*;
import java.time.Instant;
@Entity
@Table(name = "detalle_historia")
public class DetailHC {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cons_id")
    private long detailHCID;
    @Column(name = "cons_fecha")
    private Instant detailHCDate;
    @Column(name = "cons_estado")
    private String detailHCstate;

    @OneToOne
    @JoinColumn(name = "fk_hc_id" , referencedColumnName = "hc_id")
    private HistoryClinic historyClinic;

    @OneToOne
    @JoinColumn(name = "fk_prof_id" , referencedColumnName = "prof_id")
    private Professional professional;

   public DetailHC (){
       // Constructor
   }

    public long getDetailHCID() {
        return detailHCID;
    }

    public void setDetailHCID(long detailHCID) {
        this.detailHCID = detailHCID;
    }

    public Instant getDetailHCDate() {
        return detailHCDate;
    }

    public void setDetailHCDate(Instant detailHCDate) {
        this.detailHCDate = detailHCDate;
    }

    public String getDetailHCstate() {
        return detailHCstate;
    }

    public void setDetailHCstate(String detailHCstate) {
        this.detailHCstate = detailHCstate;
    }

    public HistoryClinic getHistoryClinic() {
        return historyClinic;
    }

    public void setHistoryClinic(HistoryClinic historyClinic) {
        this.historyClinic = historyClinic;
    }

    public Professional getProfessional() {
        return professional;
    }

    public void setProfessional(Professional professional) {
        this.professional = professional;
    }
}
