package com.dhr.smilesoft.model;

import javax.persistence.*;

@Entity
@Table(name = "eps")
public class Eps {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "eps_id")
    private long epsID;
    @Column(name = "eps_desc")
    private String description;
    @Column(name = "eps_sigla")
    private String sigla;




    public Eps(){
        // Constructor
    }

    public long getEpsID() {
        return epsID;
    }

    public void setEpsID(long epsID) {
        this.epsID = epsID;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }


}
