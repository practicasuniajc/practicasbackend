package com.dhr.smilesoft.model;

import javax.persistence.*;

@Entity
@Table(name = "arquitectura_gingival")
public class GingivalArchitecture {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ag_id")
    private Long gaId;
    @Column(name = "ag_papila_espinterd")
    private boolean papilaEspinterd;
    @Column(name = "ag_papila_achatada")
    private boolean papilaFlat;
    @Column(name = "ag_papila_aplanada")
    private boolean papilaFlattened;
    @Column(name = "ag_crateriforme")
    private boolean crateriform;
    @Column(name = "ag_papila_otra")
    private String papilaOther;
    @Column(name = "ag_marg_filo")
    private boolean edgeMargin;
    @Column(name = "ag_marg_redondo")
    private boolean roundMargin;
    @Column(name = "ag_marg_retraido")
    private boolean retractedMargin;
    @Column(name = "ag_marg_otra")
    private String otherMargin;
    @Column(name = "ag_marg_desc")
    private String marginDescription;
    @Column(name = "ag_encia_adherida")
    private boolean gumAttached;
    @Column(name = "ag_encia_distancia")
    private String gumAttacDist;
    @Column(name = "ag_encia_desc")
    private String gumAttacDescript;
    @Column(name = "ag_biotipo_period")
    private boolean biotypePeriod;
    @Column(name = "ag_biotipo_desc")
    private String biotypePeriodDescript;
    @OneToOne
    @JoinColumn(name = "fk_periodon_id" , referencedColumnName = "per_id")
    Periodontics periodontics;

    public GingivalArchitecture() {
        // Costructor
    }

    public Long getGaId() { return gaId; }

    public void setGaId(Long gaId) { this.gaId = gaId; }

    public boolean isPapilaEspinterd() {
        return papilaEspinterd;
    }

    public void setPapilaEspinterd(boolean papilaEspinterd) {
        this.papilaEspinterd = papilaEspinterd;
    }

    public boolean isPapilaFlat() {
        return papilaFlat;
    }

    public void setPapilaFlat(boolean papilaFlat) {
        this.papilaFlat = papilaFlat;
    }

    public boolean isPapilaFlattened() {
        return papilaFlattened;
    }

    public void setPapilaFlattened(boolean papilaFlattened) {
        this.papilaFlattened = papilaFlattened;
    }

    public boolean isCrateriform() {
        return crateriform;
    }

    public void setCrateriform(boolean crateriform) {
        this.crateriform = crateriform;
    }

    public String getPapilaOther() {
        return papilaOther;
    }

    public void setPapilaOther(String papilaOther) {
        this.papilaOther = papilaOther;
    }

    public boolean isEdgeMargin() {
        return edgeMargin;
    }

    public void setEdgeMargin(boolean edgeMargin) {
        this.edgeMargin = edgeMargin;
    }

    public boolean isRoundMargin() {
        return roundMargin;
    }

    public void setRoundMargin(boolean roundMargin) {
        this.roundMargin = roundMargin;
    }

    public boolean isRetractedMargin() {
        return retractedMargin;
    }

    public void setRetractedMargin(boolean retractedMargin) {
        this.retractedMargin = retractedMargin;
    }

    public String getOtherMargin() {
        return otherMargin;
    }

    public void setOtherMargin(String otherMargin) {
        this.otherMargin = otherMargin;
    }

    public String getMarginDescription() {
        return marginDescription;
    }

    public void setMarginDescription(String marginDescription) {
        this.marginDescription = marginDescription;
    }

    public boolean isGumAttached() {
        return gumAttached;
    }

    public void setGumAttached(boolean gumAttached) {
        this.gumAttached = gumAttached;
    }

    public String getGumAttacDist() {
        return gumAttacDist;
    }

    public void setGumAttacDist(String gumAttacDist) {
        this.gumAttacDist = gumAttacDist;
    }

    public String getGumAttacDescript() {
        return gumAttacDescript;
    }

    public void setGumAttacDescript(String gumAttacDescript) {
        this.gumAttacDescript = gumAttacDescript;
    }

    public boolean isBiotypePeriod() {
        return biotypePeriod;
    }

    public void setBiotypePeriod(boolean biotypePeriod) {
        this.biotypePeriod = biotypePeriod;
    }

    public String getBiotypePeriodDescript() {
        return biotypePeriodDescript;
    }

    public void setBiotypePeriodDescript(String biotypePeriodDescript) { this.biotypePeriodDescript = biotypePeriodDescript; }

    public Periodontics getPeriodontics() {
        return periodontics;
    }

    public void setPeriodontics(Periodontics periodontics) {
        this.periodontics = periodontics;
    }
}
