package com.dhr.smilesoft.model;

import javax.persistence.*;

@Entity
@Table(name = "descrip_gingival")
public class GingivalDescription {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "dg_id")
    private Long gdId;
    @Column(name = "dg_color")
    private String color;
    @Column(name = "dg_desc_color")
    private String colorDescription;
    @Column(name = "dg_consistencia")
    private String consistency;
    @Column(name = "dg_desc_consistenc")
    private String consistDescript;
    @Column(name = " dg_aspecto")
    private String appearance;
    @Column(name = "dg_desc_aspecto")
    private String appearanceDescript;
    @OneToOne
    @JoinColumn(name = "fk_periodon_id" , referencedColumnName = "per_id")
    private Periodontics periodontics;

    public GingivalDescription() {
        // Construco
    }

    public Long getGdId() {
        return gdId;
    }

    public void setGdId(Long gdId) {
        this.gdId = gdId;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getColorDescription() {
        return colorDescription;
    }

    public void setColorDescription(String colorDescription) {
        this.colorDescription = colorDescription;
    }

    public String getConsistency() {
        return consistency;
    }

    public void setConsistency(String consistency) {
        this.consistency = consistency;
    }

    public String getConsistDescript() {
        return consistDescript;
    }

    public void setConsistDescript(String consistDescript) {
        this.consistDescript = consistDescript;
    }

    public String getAppearance() {
        return appearance;
    }

    public void setAppearance(String appearance) {
        this.appearance = appearance;
    }

    public String getAppearanceDescript() {
        return appearanceDescript;
    }

    public void setAppearanceDescript(String appearanceDescript) {
        this.appearanceDescript = appearanceDescript;
    }

    public Periodontics getPeriodontics() {
        return periodontics;
    }

    public void setPeriodontics(Periodontics periodontics) {
        this.periodontics = periodontics;
    }
}
