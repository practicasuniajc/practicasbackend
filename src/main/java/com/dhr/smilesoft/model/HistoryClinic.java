package com.dhr.smilesoft.model;

import javax.persistence.*;

@Entity
@Table(name = "historia_clinica")
public class HistoryClinic {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "hc_id")
    private Long historyClinicID;

    @OneToOne
    @JoinColumn(name = "fk_pac_dni" , referencedColumnName = "pac_dni")
    private Pacient pacien;

    public HistoryClinic (){
        // Constructor
    }

    public Long getHistoryClinicID() { return historyClinicID; }

    public void setHistoryClinicID(Long historyClinicID) { this.historyClinicID = historyClinicID; }

    public Pacient getPacien() {
        return pacien;
    }

    public void setPacien(Pacient pacien) { this.pacien = pacien; }
}
