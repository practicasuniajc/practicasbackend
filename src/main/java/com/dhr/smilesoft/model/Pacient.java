package com.dhr.smilesoft.model;



import javax.persistence.*;
import java.time.Instant;
@Entity
@Table(name = "paciente")
public class Pacient {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pac_dni")
    private long dniPacient;
    @Column (name = "pac_nombre")
    private String name;
    @Column (name = "pac_genero")
    private String gender;
    @Column(name = "pac_ocupacion")
    private String ocupation;
    @Column(name = "pac_telefono")
    private String phone;
    @Column(name = "pac_lugar_nacimiento")
    private String placeBorn;
    @Column(name = "pac_fecha_nac")
    private Instant dateOfBorn;
    @Column(name = "pac_edad")
    private int ege;
    @Column(name = "pac_Religion")
    private String Religion;
    @Column(name = "pac_dir_residencia")
    private String addresResident;
    @Column(name = "pac_tel_residencia")
    private String phoneResident;
    @Column(name = "pac_estadocivil")
    private String stadeCivil;
    @Column(name = "pac_escolaridad")
    private String scholarship;
    @Column(name = "pac_responsable")
    private String responsable;
    @Column(name = "pac_correo")
    private String email;

    @OneToOne
    @JoinColumn(name = "fk_eps_id" , referencedColumnName = "eps_id")
    private Eps eps;

    @OneToOne
    @JoinColumn(name = "fk_td_id" , referencedColumnName = "td_id")
    private TypeDocument typeDocument;


    public Pacient (){
        // Constructor
    }

    public long getDniPacient() {
        return dniPacient;
    }

    public void setDniPacient(long dniPacient) {
        this.dniPacient = dniPacient;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getOcupation() {
        return ocupation;
    }

    public void setOcupation(String ocupation) {
        this.ocupation = ocupation;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPlaceBorn() {
        return placeBorn;
    }

    public void setPlaceBorn(String placeBorn) {
        this.placeBorn = placeBorn;
    }

    public Instant getDateOfBorn() {
        return dateOfBorn;
    }

    public void setDateOfBorn(Instant dateOfBorn) {
        this.dateOfBorn = dateOfBorn;
    }

    public int getEge() {
        return ege;
    }

    public void setEge(int ege) {
        this.ege = ege;
    }

    public String getReligion() {
        return Religion;
    }

    public void setReligion(String religion) {
        Religion = religion;
    }

    public String getAddresResident() {
        return addresResident;
    }

    public void setAddresResident(String addresResident) {
        this.addresResident = addresResident;
    }

    public String getPhoneResident() {
        return phoneResident;
    }

    public void setPhoneResident(String phoneResident) {
        this.phoneResident = phoneResident;
    }

    public String getStadeCivil() {
        return stadeCivil;
    }

    public void setStadeCivil(String stadeCivil) {
        this.stadeCivil = stadeCivil;
    }

    public String getScholarship() {
        return scholarship;
    }

    public void setScholarship(String scholarship) {
        this.scholarship = scholarship;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Eps getEps() {
        return eps;
    }

    public void setEps(Eps eps) {
        this.eps = eps;
    }

    public TypeDocument getTypeDocument() {
        return typeDocument;
    }

    public void setTypeDocument(TypeDocument typeDocument) {
        this.typeDocument = typeDocument;
    }
}
