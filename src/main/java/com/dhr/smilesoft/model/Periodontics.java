package com.dhr.smilesoft.model;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "periodoncia")
public class Periodontics {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "per_id")
    private Long pId;

    @Column(name = "per_fecha")
    private Instant date;

    @OneToOne
    @JoinColumn(name = "fk_detalle_hc_id" , referencedColumnName = "cons_id")
    private DetailHC detail;

    public Periodontics() {
        // Costructor
    }

    public Long getpId() {
        return pId;
    }

    public void setpId(Long pId) {
        this.pId = pId;
    }

    public Instant getDate() {
        return date;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public DetailHC getDetail() { return detail; }

    public void setDetail(DetailHC detail) { this.detail = detail; }
}
