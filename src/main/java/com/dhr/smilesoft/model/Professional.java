package com.dhr.smilesoft.model;



import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "profesional")
public class Professional {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "prof_id")
    private long professionalID;
    @Column(name = "prof_nombre")
    private String name;
    @Column(name = "prof_fechanacimiento")
    private Instant dateOfBorn;
    @Column(name = "prof_dni")
    private String professionalDNI;
    @Column(name = "prof_celular")
    private String phone;
    @Column(name = "prof_telef_casa")
    private String phoneHome;
    @Column(name = "prof_telef_oficina")
    private  String phoneOffice;
    @Column(name = "prof_correo")
    private String email;
    @Column(name = "prof_dir_ofic")
    private String addresOfice;
    @Column(name = "prof_dir_casa")
    private String addresHome;

    @OneToOne
    @JoinColumn(name = "fk_td_id" , referencedColumnName = "td_id")
    private TypeDocument typeDocument;

    public Professional (){
        // Constructor
    }

    public long getProfessionalID() {
        return professionalID;
    }

    public void setProfessionalID(long professionalID) {
        this.professionalID = professionalID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Instant getDateOfBorn() {
        return dateOfBorn;
    }

    public void setDateOfBorn(Instant dateOfBorn) {
        this.dateOfBorn = dateOfBorn;
    }

    public String getProfessionalDNI() {
        return professionalDNI;
    }

    public void setProfessionalDNI(String professionalDNI) {
        this.professionalDNI = professionalDNI;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhoneHome() {
        return phoneHome;
    }

    public void setPhoneHome(String phoneHome) {
        this.phoneHome = phoneHome;
    }

    public String getPhoneOffice() {
        return phoneOffice;
    }

    public void setPhoneOffice(String phoneOffice) {
        this.phoneOffice = phoneOffice;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddresOfice() {
        return addresOfice;
    }

    public void setAddresOfice(String addresOfice) {
        this.addresOfice = addresOfice;
    }

    public String getAddresHome() {
        return addresHome;
    }

    public void setAddresHome(String addresHome) {
        this.addresHome = addresHome;
    }

    public TypeDocument getTypeDocument() {
        return typeDocument;
    }

    public void setTypeDocument(TypeDocument typeDocument) {
        this.typeDocument = typeDocument;
    }
}