package com.dhr.smilesoft.model;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name="rx_estruc_anatomica")
public class RxAnatomicalStructure {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="rxea_id")
    private Long asId;
    @Column(name="rxes_date")
    private Instant date;
    @Column(name="rxes_orbit")
    private Boolean orbit;
    @Column(name="rxes_septum")
    private Boolean septum;
    @Column(name="rxes_spine")
    private Boolean spine;
    @Column(name="rxes_hard_palate")
    private Boolean hardPalate;
    @Column(name="rxes_seno_maxillary")
    private Boolean senoMaxillary;
    @Column(name="rxes_malar")
    private Boolean malar;
    @Column(name="rxes_tuberosity")
    private Boolean tuberosity;
    @Column(name="rxes_eminartic")
    private Boolean eminartic;
    @Column(name="rxes_fglenoide")
    private Boolean fglenoide;
    @Column(name="rxes_condyle")
    private Boolean condyle;
    @Column(name="rxes_esigmoidea")
    private Boolean esigmoidea;
    @Column(name="rxes_apofcoro")
    private Boolean apofcoro;
    @Column(name="rxes_mandibular_branch")
    private Boolean mandibularBranch;
    @Column(name="rxes_mandibular_body")
    private Boolean mandibularBody;
    @Column(name="rxes_mandibular_ang")
    private Boolean mandibularAng;
    @Column(name="rxes_rinfmandib")
    private Boolean rinfmandib;
    @Column(name="rxes_imageneology")
    private String imageneology;

    public RxAnatomicalStructure() {
        // constructor
        }

    public Long getAsId() { return asId; }
    public void setAsId(Long asId) { this.asId = asId; }

    public Instant getDate() { return date; }
    public void setDate(Instant date) { this.date = date; }

    public Boolean getOrbit() { return orbit; }
    public void setOrbit(Boolean orbit) { this.orbit = orbit; }

    public Boolean getSeptum() { return septum; }
    public void setSeptum(Boolean septum) { this.septum = septum; }

    public Boolean getSpine() { return spine; }
    public void setSpine(Boolean spine) { this.spine = spine; }

    public Boolean getHardPalate() { return hardPalate; }
    public void setHardPalate(Boolean hardPalate) { this.hardPalate = hardPalate; }

    public Boolean getSenoMaxillary() { return senoMaxillary; }
    public void setSenoMaxillary(Boolean senoMaxillary) { this.senoMaxillary = senoMaxillary; }

    public Boolean getMalar() { return malar; }
    public void setMalar(Boolean malar) { this.malar = malar; }

    public Boolean getTuberosity() {
        return tuberosity;
    }
    public void setTuberosity(Boolean tuberosity) { this.tuberosity = tuberosity; }

    public Boolean getEminartic() { return eminartic; }
    public void setEminartic(Boolean eminartic) { this.eminartic = eminartic; }

    public Boolean getFglenoide() { return fglenoide; }
    public void setFglenoide(Boolean fglenoide) { this.fglenoide = fglenoide; }

    public Boolean getCondyle() { return condyle; }
    public void setCondyle(Boolean condyle) { this.condyle = condyle; }

    public Boolean getEsigmoidea() { return esigmoidea; }
    public void setEsigmoidea(Boolean esigmoidea) { this.esigmoidea = esigmoidea; }

    public Boolean getApofcoro() { return apofcoro; }
    public void setApofcoro(Boolean apofcoro) { this.apofcoro = apofcoro; }

    public Boolean getMandibularBranch() { return mandibularBranch; }
    public void setMandibularBranch(Boolean mandibularBranch) { this.mandibularBranch = mandibularBranch; }

    public Boolean getMandibularBody() { return mandibularBody; }
    public void setMandibularBody(Boolean mandibularBody) { this.mandibularBody = mandibularBody; }

    public Boolean getMandibularAng() { return mandibularAng; }
    public void setMandibularAng(Boolean mandibularAng) { this.mandibularAng = mandibularAng; }

    public Boolean getRinfmandib() { return rinfmandib; }
    public void setRinfmandib(Boolean rinfmandib) { this.rinfmandib = rinfmandib; }

    public String getImageneology() { return imageneology; }
    public void setImageneology(String imageneology) { this.imageneology = imageneology; }
}
