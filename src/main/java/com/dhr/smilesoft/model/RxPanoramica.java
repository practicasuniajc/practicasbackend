package com.dhr.smilesoft.model;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name="rx_panoramica")
public class RxPanoramica {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="rxpa_id")
    private Long paId;

    @Column(name="rxpa_fecha_toma")
    private Instant date;

    @Column(name="rxpa_maxsup_basal")
    private Boolean maxsupBasal;

    @Column(name="rxpa_maxsup_alveolar")
    private Boolean maxsupAlveolar;

    @Column(name="rxpa_maxinf_basal")
    private Boolean maxinfBasal;

    @Column(name="rxpa_maxinf_alveolar")
    private Boolean maxinfAlveolar;

    @Column(name="rxpa_maxi_observ")
    private String maxillaryObserv;

    @Column(name="rxpa_tipo_dentic")
    private String typeDentic;

    @Column(name="rxpa_cant_dientes")
    private int amountTeeth;

    @Column(name="rxpa_dientes_erupc")
    private int eruptedTeeth;

    @Column(name="rxpa_dientes_sinerup")
    private int teethnotErupted;

    @Column(name="rxpa_cant_dient_c1")
    private int amountTeethC1;

    @Column(name="rxpa_cant_dient_c2")
    private int amountTeethC2;

    @Column(name="rxpa_cant_dient_c3")
    private int amountTeethC3;

    @Column(name="rxpa_cant_dient_c4")
    private int amountTeethC4;

    @Column(name="rxpa_anomalia_dent")
    private Boolean dentalAnomaly;

    @Column(name="rxpa_anom_tam")
    private String sizeAnomaly;

    @Column(name="rxpa_anom_num")
    private int numberAnomaly;

    @Column(name="rxpa_anom_forma")
    private String shapeAnomaly;

    @Column(name="rxpa_anom_posic")
    private String positionAnomaly;

    @Column(name="rxpa_anom_estruct")
    private String structureAnomaly;

    @Column(name="rxpa_anom_observ")
    private String observAnomaly;

    @OneToOne
    @JoinColumn(name="fk_rx_anatomical_structure_id" , referencedColumnName = "rxea_id")
    private RxAnatomicalStructure rxAnatomicalStructure;

    @OneToOne
    @JoinColumn(name="fk_detalle_hc_id" , referencedColumnName = "cons_id")
    private DetailHC detailHC;

    public RxPanoramica() {
        // constructor
    }

    public Long getPaId() { return paId;}
    public void setPaId(Long paId) { this.paId = paId;}

    public Boolean getMaxsupBasal() { return maxsupBasal;}
    public void setMaxsupBasal(Boolean maxsupBasal) { this.maxsupBasal = maxsupBasal;}

    public Boolean getMaxsupAlveolar() {return maxsupAlveolar;}
    public void setMaxsupAlveolar(Boolean maxsupAlveolar) { this.maxsupAlveolar = maxsupAlveolar;}

    public Boolean getMaxinfBasal() {return maxinfBasal;}
    public void setMaxinfBasal(Boolean maxinfBasal) { this.maxinfBasal = maxinfBasal;}

    public Boolean getMaxinfAlveolar() { return maxinfAlveolar;}
    public void setMaxinfAlveolar(Boolean maxinfAlveolar) { this.maxinfAlveolar = maxinfAlveolar;}

    public String getMaxillaryObserv() { return maxillaryObserv;}
    public void setMaxillaryObserv(String maxillaryObserv) { this.maxillaryObserv = maxillaryObserv;}

    public String getTypeDentic() { return typeDentic;}
    public void setTypeDentic(String typeDentic) { this.typeDentic = typeDentic;}

    public int getAmountTeeth() { return amountTeeth;}
    public void setAmountTeeth(int amountTeeth) { this.amountTeeth = amountTeeth;}

    public int getEruptedTeeth() { return eruptedTeeth;}
    public void setEruptedTeeth(int eruptedTeeth) { this.eruptedTeeth = eruptedTeeth;}

    public int getTeethnotErupted() { return teethnotErupted;}
    public void setTeethnotErupted(int teethnotErupted) { this.teethnotErupted = teethnotErupted;}

    public int getAmountTeethC1() { return amountTeethC1;}
    public void setAmountTeethC1(int amountTeethC1) { this.amountTeethC1 = amountTeethC1;}

    public int getAmountTeethC2() { return amountTeethC2;}
    public void setAmountTeethC2(int amountTeethC2) { this.amountTeethC2 = amountTeethC2;}

    public int getAmountTeethC3() { return amountTeethC3;}
    public void setAmountTeethC3(int amountTeethC3) { this.amountTeethC3 = amountTeethC3;}

    public int getAmountTeethC4() { return amountTeethC4;}
    public void setAmountTeethC4(int amountTeethC4) { this.amountTeethC4 = amountTeethC4;}

    public Boolean getDentalAnomaly() { return dentalAnomaly;}
    public void setDentalAnomaly(Boolean dentalAnomaly) { this.dentalAnomaly = dentalAnomaly;}

    public String getSizeAnomaly() { return sizeAnomaly;}
    public void setSizeAnomaly(String sizeAnomaly) { this.sizeAnomaly = sizeAnomaly;}

    public int getNumberAnomaly() { return numberAnomaly;}
    public void setNumberAnomaly(int numberAnomaly) { this.numberAnomaly = numberAnomaly;}

    public String getShapeAnomaly() { return shapeAnomaly;}
    public void setShapeAnomaly(String shapeAnomaly) { this.shapeAnomaly = shapeAnomaly;}

    public String getPositionAnomaly() { return positionAnomaly;}
    public void setPositionAnomaly(String positionAnomaly) { this.positionAnomaly = positionAnomaly;}

    public String getStructureAnomaly() { return structureAnomaly;}
    public void setStructureAnomaly(String structureAnomaly) { this.structureAnomaly = structureAnomaly;}

    public String getObservAnomaly() { return observAnomaly;}
    public void setObservAnomaly(String observAnomaly) { this.observAnomaly = observAnomaly;}

    public RxAnatomicalStructure getRxAnatomicalStructure() { return rxAnatomicalStructure;}
    public void setRxAnatomicalStructure(RxAnatomicalStructure rxAnatomicalStructure) { this.rxAnatomicalStructure = rxAnatomicalStructure;}

    public DetailHC getDetailHC() { return detailHC;}
    public void setDetailHC(DetailHC detailHC) { this.detailHC = detailHC;}
}
