package com.dhr.smilesoft.model;



import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "rx_periapical")
public class RxPeriapical {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "rxper_id")
    private long pID;
    @Column(name = "rxper_fecha")
    private Instant date;
    @Column(name = "rxper_diente")
    private int tooth;
    @Column(name = "rxper_cortical")
    private String cordical;
    @Column(name = "rxper_cresta_osea")
    private String boneCrest;
    @Column(name = "rxper_corona")
    private String corona;
    @Column(name = "rxper_cam_conductpul")
    private String campCodutal;
    @Column(name = "rxper_furca")
    private String furca;
    @Column(name = "rxper_raices")
    private String root;
    @Column(name = "rxper_zona_apical")
    private String apicalZone;
    @Column(name = "rxper_perdiosea_tipo")
    private String typeLoseOsea;
    @Column(name = "rxper_perdiosea_grado")
    private String gradeLoseOsea;
    @Column(name = "rxper_rel_coronaraiz")
    private String relCoronaRoot;
    @Column(name = "rxper_espacio_ligaperiod")
    private String spaceLigperiod;
    @Column(name = "rxper_operador")
    private  int operator;
    @OneToOne
    @JoinColumn(name = "fk_detalle_hc_id" , referencedColumnName = "cons_id")
    private DetailHC detailHC;


    public RxPeriapical() {
        // Constructor
    }


    public long getpID() {
        return pID;
    }

    public void setpID(long pID) {
        this.pID = pID;
    }

    public Instant getDate() {
        return date;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public int getTooth() {
        return tooth;
    }

    public void setTooth(int tooth) {
        this.tooth = tooth;
    }

    public String getCordical() {
        return cordical;
    }

    public void setCordical(String cordical) {
        this.cordical = cordical;
    }

    public String getBoneCrest() {
        return boneCrest;
    }

    public void setBoneCrest(String boneCrest) {
        this.boneCrest = boneCrest;
    }

    public String getCorona() {
        return corona;
    }

    public void setCorona(String corona) {
        this.corona = corona;
    }

    public String getCampCodutal() {
        return campCodutal;
    }

    public void setCampCodutal(String campCodutal) {
        this.campCodutal = campCodutal;
    }

    public String getFurca() {
        return furca;
    }

    public void setFurca(String furca) {
        this.furca = furca;
    }

    public String getRoot() {
        return root;
    }

    public void setRoot(String root) {
        this.root = root;
    }

    public String getApicalZone() {
        return apicalZone;
    }

    public void setApicalZone(String apicalZone) {
        this.apicalZone = apicalZone;
    }

    public String getTypeLoseOsea() {
        return typeLoseOsea;
    }

    public void setTypeLoseOsea(String typeLoseOsea) {
        this.typeLoseOsea = typeLoseOsea;
    }

    public String getGradeLoseOsea() {
        return gradeLoseOsea;
    }

    public void setGradeLoseOsea(String gradeLoseOsea) {
        this.gradeLoseOsea = gradeLoseOsea;
    }

    public String getRelCoronaRoot() {
        return relCoronaRoot;
    }

    public void setRelCoronaRoot(String relCoronaRoot) {
        this.relCoronaRoot = relCoronaRoot;
    }

    public String getSpaceLigperiod() {
        return spaceLigperiod;
    }

    public void setSpaceLigperiod(String spaceLigperiod) {
        this.spaceLigperiod = spaceLigperiod;
    }

    public int getOperator() {
        return operator;
    }

    public void setOperator(int operator) {
        this.operator = operator;
    }

    public DetailHC getDetailHC() {
        return detailHC;
    }

    public void setDetailHC(DetailHC detailHC) {
        this.detailHC = detailHC;
    }
}
