package com.dhr.smilesoft.model;

import javax.persistence.*;


@Entity
@Table(name = "tipo_documento")
public class TypeDocument {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "td_id")
    private long typeID;
    @Column(name = "td_desc")
    private String decription;
    @Column(name = "td_sigla")
    private String sigla;


    public TypeDocument(){
        // Constructor
    }

    public long getTypeID() {
        return typeID;
    }

    public void setTypeID(long typeID) {
        this.typeID = typeID;
    }

    public String getDecription() {
        return decription;
    }

    public void setDecription(String decription) {
        this.decription = decription;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }
}
