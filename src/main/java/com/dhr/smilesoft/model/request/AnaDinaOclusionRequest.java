package com.dhr.smilesoft.model.request;

public class AnaDinaOclusionRequest {

    private Long adoId;
    private boolean deflection;
    private String deflectOrientation;
    private String deflectMagnit;
    private String deflectContact;
    private String protrusExcursion;
    private String fchristensen;
    private String rightSideExcur;
    private String rightProtecan;
    private String rightNotWorkCont;
    private String leftSideExcur;
    private String leftProtecan;
    private String lefttNotWorkCont;
    private String observations;
    private String date;
    private DetailHCRequest detailHC;

    public AnaDinaOclusionRequest() {
        //constuctor
    }

    public Long getAdoId() {
        return adoId;
    }

    public void setAdoId(Long adoId) {
        this.adoId = adoId;
    }

    public boolean isDeflection() {
        return deflection;
    }

    public void setDeflection(boolean deflection) {
        this.deflection = deflection;
    }

    public String getDeflectOrientation() {
        return deflectOrientation;
    }

    public void setDeflectOrientation(String deflectOrientation) {
        this.deflectOrientation = deflectOrientation;
    }

    public String getDeflectMagnit() {
        return deflectMagnit;
    }

    public void setDeflectMagnit(String deflectMagnit) {
        this.deflectMagnit = deflectMagnit;
    }

    public String getDeflectContact() {
        return deflectContact;
    }

    public void setDeflectContact(String deflectContact) {
        this.deflectContact = deflectContact;
    }

    public String getProtrusExcursion() {
        return protrusExcursion;
    }

    public void setProtrusExcursion(String protrusExcursion) {
        this.protrusExcursion = protrusExcursion;
    }

    public String getFchristensen() {
        return fchristensen;
    }

    public void setFchristensen(String fchristensen) {
        this.fchristensen = fchristensen;
    }

    public String getRightSideExcur() {
        return rightSideExcur;
    }

    public void setRightSideExcur(String rightSideExcur) {
        this.rightSideExcur = rightSideExcur;
    }

    public String getRightProtecan() {
        return rightProtecan;
    }

    public void setRightProtecan(String rightProtecan) {
        this.rightProtecan = rightProtecan;
    }

    public String getRightNotWorkCont() {
        return rightNotWorkCont;
    }

    public void setRightNotWorkCont(String rightNotWorkCont) {
        this.rightNotWorkCont = rightNotWorkCont;
    }

    public String getLeftSideExcur() {
        return leftSideExcur;
    }

    public void setLeftSideExcur(String leftSideExcur) {
        this.leftSideExcur = leftSideExcur;
    }

    public String getLeftProtecan() {
        return leftProtecan;
    }

    public void setLeftProtecan(String leftProtecan) {
        this.leftProtecan = leftProtecan;
    }

    public String getLefttNotWorkCont() {
        return lefttNotWorkCont;
    }

    public void setLefttNotWorkCont(String lefttNotWorkCont) {
        this.lefttNotWorkCont = lefttNotWorkCont;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public DetailHCRequest getDetailHC() {
        return detailHC;
    }

    public void setDetailHC(DetailHCRequest detailHC) {
        this.detailHC = detailHC;
    }
}
