package com.dhr.smilesoft.model.request;

public class CharactClinicalExamRequest {

    private Long ceId;
    boolean generalBleending;
    boolean localBleending;
    boolean stimulusBleending;
    boolean spontaneousBleending;
    String descriptBleending;
    boolean exudate;
    String descriptExudate;
    boolean mobility;
    String descriptMobility;
    String gradeMobility;
    int lesionFurca;
    String descripFurca;
    boolean toothBag;
    String descriptToothBag;
    boolean calculation;
    String typeCalculation;
    String descriptCalculation;
    PeriodonticsRequest periodonticsRequest;

    public CharactClinicalExamRequest() {
        // Costructor
    }

    public Long getCeId() {
        return ceId;
    }

    public void setCeId(Long ceId) {
        this.ceId = ceId;
    }

    public boolean isGeneralBleending() {
        return generalBleending;
    }

    public void setGeneralBleending(boolean generalBleending) {
        this.generalBleending = generalBleending;
    }

    public boolean isLocalBleending() {
        return localBleending;
    }

    public void setLocalBleending(boolean localBleending) {
        this.localBleending = localBleending;
    }

    public boolean isStimulusBleending() {
        return stimulusBleending;
    }

    public void setStimulusBleending(boolean stimulusBleending) {
        this.stimulusBleending = stimulusBleending;
    }

    public boolean isSpontaneousBleending() {
        return spontaneousBleending;
    }

    public void setSpontaneousBleending(boolean spontaneousBleending) {this.spontaneousBleending = spontaneousBleending; }

    public String getDescriptBleending() {
        return descriptBleending;
    }

    public void setDescriptBleending(String descriptBleending) {
        this.descriptBleending = descriptBleending;
    }

    public boolean isExudate() {
        return exudate;
    }

    public void setExudate(boolean exudate) {
        this.exudate = exudate;
    }

    public String getDescriptExudate() {
        return descriptExudate;
    }

    public void setDescriptExudate(String descriptExudate) {
        this.descriptExudate = descriptExudate;
    }

    public boolean isMobility() {
        return mobility;
    }

    public void setMobility(boolean mobility) {
        this.mobility = mobility;
    }

    public String getDescriptMobility() {
        return descriptMobility;
    }

    public void setDescriptMobility(String descriptMobility) {
        this.descriptMobility = descriptMobility;
    }

    public String getGradeMobility() {
        return gradeMobility;
    }

    public void setGradeMobility(String gradeMobility) {
        this.gradeMobility = gradeMobility;
    }

    public int getLesionFurca() {
        return lesionFurca;
    }

    public void setLesionFurca(int lesionFurca) {
        this.lesionFurca = lesionFurca;
    }

    public String getDescripFurca() {
        return descripFurca;
    }

    public void setDescripFurca(String descripFurca) {
        this.descripFurca = descripFurca;
    }

    public boolean isToothBag() {
        return toothBag;
    }

    public void setToothBag(boolean toothBag) {
        this.toothBag = toothBag;
    }

    public String getDescriptToothBag() {
        return descriptToothBag;
    }

    public void setDescriptToothBag(String descriptToothBag) {
        this.descriptToothBag = descriptToothBag;
    }

    public boolean isCalculation() {
        return calculation;
    }

    public void setCalculation(boolean calculation) {
        this.calculation = calculation;
    }

    public String getTypeCalculation() {
        return typeCalculation;
    }

    public void setTypeCalculation(String typeCalculation) {
        this.typeCalculation = typeCalculation;
    }

    public String getDescriptCalculation() {
        return descriptCalculation;
    }

    public void setDescriptCalculation(String descriptCalculation) {
        this.descriptCalculation = descriptCalculation;
    }

    public PeriodonticsRequest getPeriodonticsRequest() {
        return periodonticsRequest;
    }

    public void setPeriodonticsRequest(PeriodonticsRequest periodonticsRequest) { this.periodonticsRequest = periodonticsRequest; }
}
