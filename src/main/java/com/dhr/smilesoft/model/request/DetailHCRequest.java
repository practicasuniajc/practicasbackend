package com.dhr.smilesoft.model.request;

import java.time.Instant;

public class DetailHCRequest {
    private Long detailHCID;
    private Instant detailHCDate;
    private String detailHCstate;
    private HistoryClinicRequest historyClinicRequest;
    private ProfessionalRequest professionalRequest;

    public  DetailHCRequest ( ){
        // Constructor
    }

    public Long getDetailHCID() {
        return detailHCID;
    }

    public void setDetailHCID(Long detailHCID) {
        this.detailHCID = detailHCID;
    }

    public Instant getDetailHCDate() {
        return detailHCDate;
    }

    public void setDetailHCDate(Instant detailHCDate) {
        this.detailHCDate = detailHCDate;
    }

    public String getDetailHCstate() {
        return detailHCstate;
    }

    public void setDetailHCstate(String detailHCstate) {
        this.detailHCstate = detailHCstate;
    }

    public HistoryClinicRequest getHistoryClinicRequest() {
        return historyClinicRequest;
    }

    public void setHistoryClinicRequest(HistoryClinicRequest historyClinicRequest) { this.historyClinicRequest = historyClinicRequest; }

    public ProfessionalRequest getProfessionalRequest() {
        return professionalRequest;
    }

    public void setProfessionalRequest(ProfessionalRequest professionalRequest) { this.professionalRequest = professionalRequest; }
}
