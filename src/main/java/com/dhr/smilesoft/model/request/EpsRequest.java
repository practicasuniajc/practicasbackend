package com.dhr.smilesoft.model.request;

public class EpsRequest {

    private long epsID;
    private String description;
    private String sigla;


    public EpsRequest(){
        // Constructor
    }

    public long getEpsID() {
        return epsID;
    }

    public void setEpsID(long epsID) {
        this.epsID = epsID;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }
}
