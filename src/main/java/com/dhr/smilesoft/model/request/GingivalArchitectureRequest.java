package com.dhr.smilesoft.model.request;

public class GingivalArchitectureRequest {

    private Long gaId;
    private boolean papilaEspinterd;
    private boolean papilaFlat;
    private boolean papilaFlattened;
    private boolean crateriform;
    private String papilaOther;
    private boolean edgeMargin;
    private boolean roundMargin;
    private boolean retractedMargin;
    private String otherMargin;
    private String marginDescription;
    private boolean gumAttached;
    private String gumAttacDist;
    private String gumAttacDescript;
    private boolean biotypePeriod;
    private String biotypePeriodDescript;
    PeriodonticsRequest periodonticsRequest;

    public GingivalArchitectureRequest() {
        // Costructor
    }

    public Long getGaId() {
        return gaId;
    }

    public void setGaId(Long gaId) {
        this.gaId = gaId;
    }

    public boolean isPapilaEspinterd() {
        return papilaEspinterd;
    }

    public void setPapilaEspinterd(boolean papilaEspinterd) {
        this.papilaEspinterd = papilaEspinterd;
    }

    public boolean isPapilaFlat() {
        return papilaFlat;
    }

    public void setPapilaFlat(boolean papilaFlat) {
        this.papilaFlat = papilaFlat;
    }

    public boolean isPapilaFlattened() {
        return papilaFlattened;
    }

    public void setPapilaFlattened(boolean papilaFlattened) {
        this.papilaFlattened = papilaFlattened;
    }

    public boolean isCrateriform() {
        return crateriform;
    }

    public void setCrateriform(boolean crateriform) {
        this.crateriform = crateriform;
    }

    public String getPapilaOther() {
        return papilaOther;
    }

    public void setPapilaOther(String papilaOther) {
        this.papilaOther = papilaOther;
    }

    public boolean isEdgeMargin() {
        return edgeMargin;
    }

    public void setEdgeMargin(boolean edgeMargin) {
        this.edgeMargin = edgeMargin;
    }

    public boolean isRoundMargin() {
        return roundMargin;
    }

    public void setRoundMargin(boolean roundMargin) {
        this.roundMargin = roundMargin;
    }

    public boolean isRetractedMargin() {
        return retractedMargin;
    }

    public void setRetractedMargin(boolean retractedMargin) {
        this.retractedMargin = retractedMargin;
    }

    public String getOtherMargin() {
        return otherMargin;
    }

    public void setOtherMargin(String otherMargin) {
        this.otherMargin = otherMargin;
    }

    public String getMarginDescription() {
        return marginDescription;
    }

    public void setMarginDescription(String marginDescription) {
        this.marginDescription = marginDescription;
    }

    public boolean isGumAttached() {
        return gumAttached;
    }

    public void setGumAttached(boolean gumAttached) {
        this.gumAttached = gumAttached;
    }

    public String getGumAttacDist() {
        return gumAttacDist;
    }

    public void setGumAttacDist(String gumAttacDist) {
        this.gumAttacDist = gumAttacDist;
    }

    public String getGumAttacDescript() {
        return gumAttacDescript;
    }

    public void setGumAttacDescript(String gumAttacDescript) {
        this.gumAttacDescript = gumAttacDescript;
    }

    public boolean isBiotypePeriod() {
        return biotypePeriod;
    }

    public void setBiotypePeriod(boolean biotypePeriod) {
        this.biotypePeriod = biotypePeriod;
    }

    public String getBiotypePeriodDescript() {
        return biotypePeriodDescript;
    }

    public void setBiotypePeriodDescript(String biotypePeriodDescript) { this.biotypePeriodDescript = biotypePeriodDescript; }

    public PeriodonticsRequest getPeriodonticsRequest() {
        return periodonticsRequest;
    }

    public void setPeriodonticsRequest(PeriodonticsRequest periodonticsRequest) { this.periodonticsRequest = periodonticsRequest; }
}
