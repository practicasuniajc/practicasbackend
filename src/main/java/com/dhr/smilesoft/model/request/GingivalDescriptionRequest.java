package com.dhr.smilesoft.model.request;

public class GingivalDescriptionRequest {

    private Long gdId;
    private String color;
    private String colorDescription;
    private String consistency;
    private String consistDescript;
    private String appearance;
    private String appearanceDescript;
    private PeriodonticsRequest periodonticsRequest;

    public GingivalDescriptionRequest() {
        // Costructor
    }

    public Long getGdId() {
        return gdId;
    }

    public void setGdId(Long gdId) {
        this.gdId = gdId;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getColorDescription() {
        return colorDescription;
    }

    public void setColorDescription(String colorDescription) {
        this.colorDescription = colorDescription;
    }

    public String getConsistency() {
        return consistency;
    }

    public void setConsistency(String consistency) {
        this.consistency = consistency;
    }

    public String getConsistDescript() {
        return consistDescript;
    }

    public void setConsistDescript(String consistDescript) {
        this.consistDescript = consistDescript;
    }

    public String getAppearance() {
        return appearance;
    }

    public void setAppearance(String appearance) {
        this.appearance = appearance;
    }

    public String getAppearanceDescript() {
        return appearanceDescript;
    }

    public void setAppearanceDescript(String appearanceDescript) {
        this.appearanceDescript = appearanceDescript;
    }

    public PeriodonticsRequest getPeriodonticsRequest() {
        return periodonticsRequest;
    }

    public void setPeriodonticsRequest(PeriodonticsRequest periodonticsRequest) { this.periodonticsRequest = periodonticsRequest; }
}
