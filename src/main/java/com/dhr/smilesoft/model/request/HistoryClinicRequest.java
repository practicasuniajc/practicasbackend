package com.dhr.smilesoft.model.request;

public class HistoryClinicRequest {

    private long historyClinicID;
    private PacientRequest pacienRequest;

    public HistoryClinicRequest (){
        // Constructor
    }

    public long getHistoryClinicID() {
        return historyClinicID;
    }

    public void setHistoryClinicID(long historyClinicID) {
        this.historyClinicID = historyClinicID;
    }

    public PacientRequest getPacienRequest() {
        return pacienRequest;
    }

    public void setPacienRequest(PacientRequest pacienRequest) {
        this.pacienRequest = pacienRequest;
    }
}
