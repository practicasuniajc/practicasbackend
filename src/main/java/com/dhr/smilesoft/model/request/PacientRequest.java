package com.dhr.smilesoft.model.request;

import java.time.Instant;

public class PacientRequest {

    private Long dniPacient;
    private String name;
    private String gender;
    private String ocupation;
    private String phone;
    private String placeBorn;
    private Instant dateOfBorn;
    private int ege;
    private String religion;
    private String addresResident;
    private int phoneResident;
    private String stadeCivil;
    private String scholarship;
    private String responsable;
    private EpsRequest epsRequest;
    private TypeDocumentRequest typeDocumentRequest;


    public PacientRequest (){
        // Constructor
    }

    public Long getDniPacient() {
        return dniPacient;
    }

    public void setDniPacient(Long dniPacient) {
        this.dniPacient = dniPacient;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getOcupation() {
        return ocupation;
    }

    public void setOcupation(String ocupation) {
        this.ocupation = ocupation;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPlaceBorn() {
        return placeBorn;
    }

    public void setPlaceBorn(String placeBorn) {
        this.placeBorn = placeBorn;
    }

    public Instant getDateOfBorn() {
        return dateOfBorn;
    }

    public void setDateOfBorn(Instant dateOfBorn) {
        this.dateOfBorn = dateOfBorn;
    }

    public int getEge() {
        return ege;
    }

    public void setEge(int ege) {
        this.ege = ege;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getAddresResident() {
        return addresResident;
    }

    public void setAddresResident(String addresResident) {
        this.addresResident = addresResident;
    }

    public int getPhoneResident() {
        return phoneResident;
    }

    public void setPhoneResident(int phoneResident) {
        this.phoneResident = phoneResident;
    }

    public String getStadeCivil() {
        return stadeCivil;
    }

    public void setStadeCivil(String stadeCivil) {
        this.stadeCivil = stadeCivil;
    }

    public String getScholarship() {
        return scholarship;
    }

    public void setScholarship(String scholarship) {
        this.scholarship = scholarship;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public EpsRequest getEpsRequest() {
        return epsRequest;
    }

    public void setEpsRequest(EpsRequest epsRequest) {
        this.epsRequest = epsRequest;
    }

    public TypeDocumentRequest getTypeDocumentRequest() {
        return typeDocumentRequest;
    }

    public void setTypeDocumentRequest(TypeDocumentRequest typeDocumentRequest) { this.typeDocumentRequest = typeDocumentRequest; }
}
