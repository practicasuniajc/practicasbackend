package com.dhr.smilesoft.model.request;

import java.time.Instant;

public class PeriodonticsRequest {
    private Long pId;
    private DetailHCRequest detailHCRequest;
    private Instant date;

    public PeriodonticsRequest(){
        // Costructor
    }

    public Long getpId() {
        return pId;
    }

    public void setpId(Long pId) {
        this.pId = pId;
    }

    public DetailHCRequest getDetailHCRequest() {
        return detailHCRequest;
    }

    public void setDetailHCRequest(DetailHCRequest detailHCRequest) {
        this.detailHCRequest = detailHCRequest;
    }

    public Instant getDate() {
        return date;
    }

    public void setDate(Instant date) {
        this.date = date;
    }
}
