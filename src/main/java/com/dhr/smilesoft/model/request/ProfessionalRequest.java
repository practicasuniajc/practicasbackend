package com.dhr.smilesoft.model.request;

import java.lang.reflect.Type;
import java.time.Instant;

public class ProfessionalRequest {

    private long professionalID;
    private String name;
    private Instant dateOfBorn;
    private String professionalDNI;
    private String phone;
    private String phoneHome;
    private String phoneOffice;
    private String email;
    private String addresOfice;
    private String addresHome;
    private TypeDocumentRequest typeDocumentRequest;

    public ProfessionalRequest (){
        // Constructor
    }

    public long getProfessionalID() {
        return professionalID;
    }

    public void setProfessionalID(long professionalID) {
        this.professionalID = professionalID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Instant getDateOfBorn() {
        return dateOfBorn;
    }

    public void setDateOfBorn(Instant dateOfBorn) {
        this.dateOfBorn = dateOfBorn;
    }

    public String getProfessionalDNI() {
        return professionalDNI;
    }

    public void setProfessionalDNI(String professionalDNI) {
        this.professionalDNI = professionalDNI;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhoneHome() {
        return phoneHome;
    }

    public void setPhoneHome(String phoneHome) {
        this.phoneHome = phoneHome;
    }

    public String getPhoneOffice() {
        return phoneOffice;
    }

    public void setPhoneOffice(String phoneOffice) {
        this.phoneOffice = phoneOffice;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddresOfice() {
        return addresOfice;
    }

    public void setAddresOfice(String addresOfice) {
        this.addresOfice = addresOfice;
    }

    public String getAddresHome() {
        return addresHome;
    }

    public void setAddresHome(String addresHome) {
        this.addresHome = addresHome;
    }

    public TypeDocumentRequest getTypeDocumentRequest() {
        return typeDocumentRequest;
    }

    public void setTypeDocumentRequest(TypeDocumentRequest typeDocumentRequest) {
        this.typeDocumentRequest = typeDocumentRequest;
    }
}
