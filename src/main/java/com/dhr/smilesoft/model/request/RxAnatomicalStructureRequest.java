package com.dhr.smilesoft.model.request;

import java.time.Instant;

public class RxAnatomicalStructureRequest {

    private Long asId;
    private Instant date;
    private Boolean orbit;
    private Boolean septum;
    private Boolean spine;
    private Boolean hardPalate;
    private Boolean senoMaxillary;
    private Boolean malar;
    private Boolean tuberosity;
    private Boolean eminartic;
    private Boolean fglenoide;
    private Boolean condyle;
    private Boolean esigmoidea;
    private Boolean apofcoro;
    private Boolean mandibularBranch;
    private Boolean mandibularBody;
    private Boolean mandibularAng;
    private Boolean rinfmandib;
    private String imageneology;

    public RxAnatomicalStructureRequest(Long asId) {
        // constructor
    }

    public Long getAsId() {
        return asId;
    }
    public void setAsId(Long asId) {
        this.asId = asId;
    }

    public Instant getDate() {
        return date;
    }
    public void setDate(Instant date) {
        this.date = date;
    }

    public Boolean getOrbit() {
        return orbit;
    }
    public void setOrbit(Boolean orbit) {
        this.orbit = orbit;
    }

    public Boolean getSeptum() {
        return septum;
    }
    public void setSeptum(Boolean septum) {
        this.septum = septum;
    }

    public Boolean getSpine() {
        return spine;
    }
    public void setSpine(Boolean spine) {
        this.spine = spine;
    }

    public Boolean getHardPalate() {
        return hardPalate;
    }
    public void setHardPalate(Boolean hardPalate) {
        this.hardPalate = hardPalate;
    }

    public Boolean getSenoMaxillary() {
        return senoMaxillary;
    }
    public void setSenoMaxillary(Boolean senoMaxillary) {
        this.senoMaxillary = senoMaxillary;
    }

    public Boolean getMalar() {
        return malar;
    }
    public void setMalar(Boolean malar) {
        this.malar = malar;
    }

    public Boolean getTuberosity() {
        return tuberosity;
    }
    public void setTuberosity(Boolean tuberosity) {
        this.tuberosity = tuberosity;
    }

    public Boolean getEminartic() {
        return eminartic;
    }
    public void setEminartic(Boolean eminartic) {
        this.eminartic = eminartic;
    }

    public Boolean getFglenoide() {
        return fglenoide;
    }
    public void setFglenoide(Boolean fglenoide) {
        this.fglenoide = fglenoide;
    }

    public Boolean getCondyle() {
        return condyle;
    }
    public void setCondyle(Boolean condyle) {
        this.condyle = condyle;
    }

    public Boolean getEsigmoidea() {
        return esigmoidea;
    }
    public void setEsigmoidea(Boolean esigmoidea) {
        this.esigmoidea = esigmoidea;
    }

    public Boolean getApofcoro() {
        return apofcoro;
    }
    public void setApofcoro(Boolean apofcoro) {
        this.apofcoro = apofcoro;
    }

    public Boolean getMandibularBranch() {
        return mandibularBranch;
    }
    public void setMandibularBranch(Boolean mandibularBranch) {
        this.mandibularBranch = mandibularBranch;
    }

    public Boolean getMandibularBody() {
        return mandibularBody;
    }
    public void setMandibularBody(Boolean mandibularBody) {
        this.mandibularBody = mandibularBody;
    }

    public Boolean getMandibularAng() {
        return mandibularAng;
    }
    public void setMandibularAng(Boolean mandibularAng) {
        this.mandibularAng = mandibularAng;
    }

    public Boolean getRinfmandib() {
        return rinfmandib;
    }
    public void setRinfmandib(Boolean rinfmandib) {
        this.rinfmandib = rinfmandib;
    }

    public String getImageneology() {
        return imageneology;
    }
    public void setImageneology(String imageneology) {
        this.imageneology = imageneology;
    }
}
