package com.dhr.smilesoft.model.request;

public class RxPanoramicaRequest {
    
    private Long paId;
    private Boolean maxsupBasal;
    private Boolean maxsupAlveolar;
    private Boolean maxinfBasal;
    private Boolean maxinfAlveolar;
    private String maxillaryObserv;
    private String typeDentic;
    private int amountTeeth;
    private int eruptedTeeth;
    private int teethnotErupted;
    private int amountTeethC1;
    private int amountTeethC2;
    private int amountTeethC3;
    private int amountTeethC4;
    private Boolean dentalAnomaly;
    private String sizeAnomaly;
    private int numberAnomaly;
    private String shapeAnomaly;
    private String positionAnomaly;
    private String structureAnomaly;
    private String observAnomaly;
    private RxAnatomicalStructureRequest rxAnatomicalStructureRequest;
    private DetailHCRequest detailHCRequest;

    public RxPanoramicaRequest(Long paId) {
        // constructor
    }

    public Long getPaId() {
        return paId;
    }

    public void setPaId(Long paId) {
        this.paId = paId;
    }

    public Boolean getMaxsupBasal() {
        return maxsupBasal;
    }

    public void setMaxsupBasal(Boolean maxsupBasal) {
        this.maxsupBasal = maxsupBasal;
    }

    public Boolean getMaxsupAlveolar() {
        return maxsupAlveolar;
    }

    public void setMaxsupAlveolar(Boolean maxsupAlveolar) {
        this.maxsupAlveolar = maxsupAlveolar;
    }

    public Boolean getMaxinfBasal() {
        return maxinfBasal;
    }

    public void setMaxinfBasal(Boolean maxinfBasal) {
        this.maxinfBasal = maxinfBasal;
    }

    public Boolean getMaxinfAlveolar() {
        return maxinfAlveolar;
    }

    public void setMaxinfAlveolar(Boolean maxinfAlveolar) {
        this.maxinfAlveolar = maxinfAlveolar;
    }

    public String getMaxillaryObserv() {
        return maxillaryObserv;
    }

    public void setMaxillaryObserv(String maxillaryObserv) {
        this.maxillaryObserv = maxillaryObserv;
    }

    public String getTypeDentic() {
        return typeDentic;
    }

    public void setTypeDentic(String typeDentic) {
        this.typeDentic = typeDentic;
    }

    public int getAmountTeeth() {
        return amountTeeth;
    }

    public void setAmountTeeth(int amountTeeth) {
        this.amountTeeth = amountTeeth;
    }

    public int getEruptedTeeth() {
        return eruptedTeeth;
    }

    public void setEruptedTeeth(int eruptedTeeth) {
        this.eruptedTeeth = eruptedTeeth;
    }

    public int getTeethnotErupted() {
        return teethnotErupted;
    }

    public void setTeethnotErupted(int teethnotErupted) {
        this.teethnotErupted = teethnotErupted;
    }

    public int getAmountTeethC1() {
        return amountTeethC1;
    }

    public void setAmountTeethC1(int amountTeethC1) {
        this.amountTeethC1 = amountTeethC1;
    }

    public int getAmountTeethC2() {
        return amountTeethC2;
    }

    public void setAmountTeethC2(int amountTeethC2) {
        this.amountTeethC2 = amountTeethC2;
    }

    public int getAmountTeethC3() {
        return amountTeethC3;
    }

    public void setAmountTeethC3(int amountTeethC3) {
        this.amountTeethC3 = amountTeethC3;
    }

    public int getAmountTeethC4() {
        return amountTeethC4;
    }

    public void setAmountTeethC4(int amountTeethC4) {
        this.amountTeethC4 = amountTeethC4;
    }

    public Boolean getDentalAnomaly() {
        return dentalAnomaly;
    }

    public void setDentalAnomaly(Boolean dentalAnomaly) {
        this.dentalAnomaly = dentalAnomaly;
    }

    public String getSizeAnomaly() {
        return sizeAnomaly;
    }

    public void setSizeAnomaly(String sizeAnomaly) {
        this.sizeAnomaly = sizeAnomaly;
    }

    public int getNumberAnomaly() {
        return numberAnomaly;
    }

    public void setNumberAnomaly(int numberAnomaly) {
        this.numberAnomaly = numberAnomaly;
    }

    public String getShapeAnomaly() {
        return shapeAnomaly;
    }

    public void setShapeAnomaly(String shapeAnomaly) {
        this.shapeAnomaly = shapeAnomaly;
    }

    public String getPositionAnomaly() {
        return positionAnomaly;
    }

    public void setPositionAnomaly(String positionAnomaly) {
        this.positionAnomaly = positionAnomaly;
    }

    public String getStructureAnomaly() {
        return structureAnomaly;
    }

    public void setStructureAnomaly(String structureAnomaly) {
        this.structureAnomaly = structureAnomaly;
    }

    public String getObservAnomaly() {
        return observAnomaly;
    }

    public void setObservAnomaly(String observAnomaly) {
        this.observAnomaly = observAnomaly;
    }

    public RxAnatomicalStructureRequest getRxAnatomicalStructureRequest() {
        return rxAnatomicalStructureRequest;
    }

    public void setRxAnatomicalStructureRequest(RxAnatomicalStructureRequest rxAnatomicalStructureRequest) {
        this.rxAnatomicalStructureRequest = rxAnatomicalStructureRequest;
    }

    public DetailHCRequest getDetailHCRequest() {
        return detailHCRequest;
    }

    public void setDetailHCRequest(DetailHCRequest detailHCRequest) {
        this.detailHCRequest = detailHCRequest;
    }
}
