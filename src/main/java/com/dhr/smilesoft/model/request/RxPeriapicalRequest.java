package com.dhr.smilesoft.model.request;

import java.time.Instant;

public class RxPeriapicalRequest {

    private Long pID;
    private Instant date;
    private int tooth;
    private String cordical;
    private String boneCrest;
    private String corona;
    private String campCodutal;
    private String furca;
    private String root;
    private String apicalZone;
    private String typeLoseOsea;
    private String gradeLoseOsea;
    private String relCoronaRoot;
    private String spaceLigperiod;
    private int operator;
    private DetailHCRequest detailHC;

    public RxPeriapicalRequest() {
        // Constructor
    }

    public Long getpID() {
        return pID;
    }

    public void setpID(Long pID) {
        this.pID = pID;
    }

    public Instant getDate() {
        return date;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public int getTooth() {
        return tooth;
    }

    public void setTooth(int tooth) {
        this.tooth = tooth;
    }

    public String getCordical() {
        return cordical;
    }

    public void setCordical(String cordical) {
        this.cordical = cordical;
    }

    public String getBoneCrest() {
        return boneCrest;
    }

    public void setBoneCrest(String boneCrest) {
        this.boneCrest = boneCrest;
    }

    public String getCorona() {
        return corona;
    }

    public void setCorona(String corona) {
        this.corona = corona;
    }

    public String getCampCodutal() {
        return campCodutal;
    }

    public void setCampCodutal(String campCodutal) {
        this.campCodutal = campCodutal;
    }

    public String getFurca() {
        return furca;
    }

    public void setFurca(String furca) {
        this.furca = furca;
    }

    public String getRoot() {
        return root;
    }

    public void setRoot(String root) {
        this.root = root;
    }

    public String getApicalZone() {
        return apicalZone;
    }

    public void setApicalZone(String apicalZone) {
        this.apicalZone = apicalZone;
    }

    public String getTypeLoseOsea() {
        return typeLoseOsea;
    }

    public void setTypeLoseOsea(String typeLoseOsea) {
        this.typeLoseOsea = typeLoseOsea;
    }

    public String getGradeLoseOsea() {
        return gradeLoseOsea;
    }

    public void setGradeLoseOsea(String gradeLoseOsea) {
        this.gradeLoseOsea = gradeLoseOsea;
    }

    public String getRelCoronaRoot() {
        return relCoronaRoot;
    }

    public void setRelCoronaRoot(String relCoronaRoot) {
        this.relCoronaRoot = relCoronaRoot;
    }

    public String getSpaceLigperiod() {
        return spaceLigperiod;
    }

    public void setSpaceLigperiod(String spaceLigperiod) {
        this.spaceLigperiod = spaceLigperiod;
    }

    public int getOperator() {
        return operator;
    }

    public void setOperator(int operator) {
        this.operator = operator;
    }

    public DetailHCRequest getDetailHC() {
        return detailHC;
    }

    public void setDetailHC(DetailHCRequest detailHC) {
        this.detailHC = detailHC;
    }
}
