package com.dhr.smilesoft.model.request;

import java.math.BigDecimal;
import java.time.Instant;

public class TableViewRequest {

    private Long id;
    private String description;
    private Instant date;
    private BigDecimal value;
    private Boolean status;

    public TableViewRequest() {
        // Constructor
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Instant getDate() {
        return date;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
