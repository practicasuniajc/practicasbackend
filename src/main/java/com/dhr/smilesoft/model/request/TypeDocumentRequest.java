package com.dhr.smilesoft.model.request;

public class TypeDocumentRequest {
    private long typeID;
    private String decription;
    private String sigla;


    public TypeDocumentRequest(){
        // Constructor
    }

    public long getTypeID() {
        return typeID;
    }

    public void setTypeID(long typeID) {
        this.typeID = typeID;
    }

    public String getDecription() {
        return decription;
    }

    public void setDecription(String decription) {
        this.decription = decription;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }
}
