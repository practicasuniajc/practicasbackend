package com.dhr.smilesoft.repository;

import com.dhr.smilesoft.model.AnaDinaOclusion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AnaDinaOclusionRepository  extends JpaRepository<AnaDinaOclusion , Long>
{
}
