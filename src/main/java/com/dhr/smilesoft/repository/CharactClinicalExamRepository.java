package com.dhr.smilesoft.repository;

import com.dhr.smilesoft.model.CharactClinicalExam;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CharactClinicalExamRepository extends JpaRepository<CharactClinicalExam, Long> {
}
