package com.dhr.smilesoft.repository;

import com.dhr.smilesoft.model.GingivalDescription;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GingivalDescriptionRepository extends JpaRepository<GingivalDescription,Long> {
}
