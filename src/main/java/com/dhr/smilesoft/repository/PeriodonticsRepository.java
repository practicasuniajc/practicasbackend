package com.dhr.smilesoft.repository;

import com.dhr.smilesoft.model.Periodontics;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PeriodonticsRepository extends JpaRepository<Periodontics, Long> {
}
