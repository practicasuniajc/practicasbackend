package com.dhr.smilesoft.repository;

import com.dhr.smilesoft.model.RxAnatomicalStructure;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RxAnatomicalStructureRepository extends JpaRepository<RxAnatomicalStructure, Long> {
}
