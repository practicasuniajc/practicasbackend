package com.dhr.smilesoft.repository;

import com.dhr.smilesoft.model.RxPeriapical;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RxPeriapicalRepository extends JpaRepository<RxPeriapical, Long> { }
