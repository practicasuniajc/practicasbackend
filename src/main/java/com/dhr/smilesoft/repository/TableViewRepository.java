package com.dhr.smilesoft.repository;

import com.dhr.smilesoft.model.TableView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TableViewRepository extends JpaRepository<TableView, Long> {

    @Query(nativeQuery = true, value = "SELECT " +
            "tb_id, " +
            "tb_descripcion, " +
            "tb_fecha, " +
            "tb_valor, " +
            "tb_estado " +
            "FROM tabla " +
            "WHERE tb_estado = :status")
    List<TableView> findByActive(@Param(value = "status") boolean status);

    TableView findByDescription(String description);

}
