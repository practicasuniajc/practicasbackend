package com.dhr.smilesoft.service;


import com.dhr.smilesoft.model.AnaDinaOclusion;
import com.dhr.smilesoft.repository.AnaDinaOclusionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AnaDinaOclusionService {

    @Autowired
    private AnaDinaOclusionRepository anaDinaOclusionRepository;

    public AnaDinaOclusion saveAnaDinaOclusion(AnaDinaOclusion anaDinaOclusion){
        return anaDinaOclusionRepository.save(anaDinaOclusion);
    }

    public AnaDinaOclusion updateAnaDinaOclusion(AnaDinaOclusion anaDinaOclusion){
        return anaDinaOclusionRepository.save(anaDinaOclusion);
    }

    public List<AnaDinaOclusion> findAll(){
        return  anaDinaOclusionRepository.findAll();
    }
    public void delectAbaDianaOclusionByID(long id){
        anaDinaOclusionRepository.deleteById(id);
    }

}
