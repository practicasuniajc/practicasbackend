package com.dhr.smilesoft.service;

import com.dhr.smilesoft.model.CharactClinicalExam;
import com.dhr.smilesoft.repository.CharactClinicalExamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CharactClinicalExamService {

    @Autowired
    private CharactClinicalExamRepository charactClinicalExamRepository;

    public CharactClinicalExam saveClinicalExam(CharactClinicalExam charactClinicalExam){
        return charactClinicalExamRepository.save(charactClinicalExam); }

    public CharactClinicalExam updateClinicalExam(CharactClinicalExam charactClinicalExam){
        return charactClinicalExamRepository.save(charactClinicalExam); }

    public List<CharactClinicalExam> findAll(){
        return charactClinicalExamRepository.findAll();
    }

    public void deleteByID(Long id){
        charactClinicalExamRepository.deleteById(id);
    }
}
