package com.dhr.smilesoft.service;

import com.dhr.smilesoft.model.GingivalArchitecture;
import com.dhr.smilesoft.repository.GingivalArchitectureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GingivalArchitectureService {

    @Autowired
    private GingivalArchitectureRepository gingivalArchitectureRepository;

    public GingivalArchitecture saveArc(GingivalArchitecture gingivalArchitecture){
        return gingivalArchitectureRepository.save(gingivalArchitecture); }

    public GingivalArchitecture updateArc(GingivalArchitecture gingivalArchitecture){
        return gingivalArchitectureRepository.save(gingivalArchitecture); }

    public List<GingivalArchitecture> findAll(){
        return gingivalArchitectureRepository.findAll();
    }

    public void deletebyID(Long id){
        gingivalArchitectureRepository.deleteById(id);
    }
}
