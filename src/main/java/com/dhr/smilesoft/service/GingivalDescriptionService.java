package com.dhr.smilesoft.service;

import com.dhr.smilesoft.model.GingivalDescription;
import com.dhr.smilesoft.repository.GingivalDescriptionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class GingivalDescriptionService {

    @Autowired
    private GingivalDescriptionRepository gingivalDescriptionRepository;

    public GingivalDescription saveGingivalDescription(GingivalDescription gingivalDescription){
        return gingivalDescriptionRepository.save(gingivalDescription); }

    public GingivalDescription updateGingivalDescription(GingivalDescription gingivalDescription){
        return gingivalDescriptionRepository.save( gingivalDescription); }

    public List<GingivalDescription> findAll(){
        return gingivalDescriptionRepository.findAll();
    }

    public void deletebyID(Long id){
        gingivalDescriptionRepository.deleteById(id);
    }
}
