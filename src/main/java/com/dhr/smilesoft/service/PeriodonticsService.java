package com.dhr.smilesoft.service;

import com.dhr.smilesoft.model.Periodontics;
import com.dhr.smilesoft.repository.PeriodonticsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class PeriodonticsService {

    @Autowired
    private PeriodonticsRepository periodonticsRepository;

    public Periodontics savePeriodontics(Periodontics periodontics){ return periodonticsRepository.save(periodontics); }

    public Periodontics updatePeriodontics(Periodontics periodontics){ return periodonticsRepository.save(periodontics); }

    public List<Periodontics> findAll(){
        return periodonticsRepository.findAll();
    }

    public void deletePeriodonticsbyID(Long id){
        periodonticsRepository.deleteById(id);
    }
}
