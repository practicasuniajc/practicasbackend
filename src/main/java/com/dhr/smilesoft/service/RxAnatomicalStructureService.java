package com.dhr.smilesoft.service;

import com.dhr.smilesoft.model.RxAnatomicalStructure;
import com.dhr.smilesoft.repository.RxAnatomicalStructureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RxAnatomicalStructureService {
    @Autowired
    private RxAnatomicalStructureRepository rxAnatomicalStructureRepository;

    public RxAnatomicalStructure saveRxAnatomicalStructure(RxAnatomicalStructure rxAnatomicalStructure){
        return rxAnatomicalStructureRepository.save(rxAnatomicalStructure);
    }
    public RxAnatomicalStructure updateRxAnatomicalStructure(RxAnatomicalStructure rxAnatomicalStructure){
        return rxAnatomicalStructureRepository.save(rxAnatomicalStructure);
    }

    public List<RxAnatomicalStructure> findAll(){
        return  rxAnatomicalStructureRepository.findAll();
    }

    public void delectRxAnatomicalStructureByID(long id){
        rxAnatomicalStructureRepository.deleteById(id);
    }
}
