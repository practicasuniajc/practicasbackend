package com.dhr.smilesoft.service;

import com.dhr.smilesoft.model.RxPanoramica;
import com.dhr.smilesoft.repository.RxPanoramicaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RxPanoramicaService {

    @Autowired
    private RxPanoramicaRepository rxPanoramicaRepository;

    public RxPanoramica saveRxPanoramica(RxPanoramica rxPanoramica){return rxPanoramicaRepository.save(rxPanoramica);}
    public RxPanoramica updateRxPanoramica(RxPanoramica rxPanoramica){return rxPanoramicaRepository.save(rxPanoramica); }
    public List<RxPanoramica> findAll(){
        return  rxPanoramicaRepository.findAll();
    }
    public void delectRxPanoramicaByID(long id){
        rxPanoramicaRepository.deleteById(id);
    }
}
