package com.dhr.smilesoft.service;

import com.dhr.smilesoft.model.RxPeriapical;
import com.dhr.smilesoft.repository.RxPeriapicalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RxPeriapicalService {

    @Autowired
    private RxPeriapicalRepository rxPeriapicalRepository;

    public RxPeriapical saveRxPeriapical(RxPeriapical rxPeriapical){
        return rxPeriapicalRepository.save(rxPeriapical); }

    public RxPeriapical UpdateRxPeriapical(RxPeriapical rxPeriapical){
        return rxPeriapicalRepository.save(rxPeriapical); }

    public List<RxPeriapical> findAll(){
        return  rxPeriapicalRepository.findAll();
    }
    public void delectRxperiapicalByID(long id){
        rxPeriapicalRepository.deleteById(id);
    }
}
