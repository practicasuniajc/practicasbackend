package com.dhr.smilesoft.service;

import com.dhr.smilesoft.model.TableView;
import com.dhr.smilesoft.repository.TableViewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TableViewService {

    @Autowired
    private TableViewRepository tableViewRepository;

    public TableView saveTableView(TableView tableView){
        return tableViewRepository.save(tableView);
    }

    public TableView updateTableView(TableView tableView){
        return tableViewRepository.save(tableView);
    }

    public List<TableView> findAll(){
        return tableViewRepository.findAll();
    }

    public List<TableView> findByStatus(boolean status){
        return tableViewRepository.findByActive(status);
    }

    public TableView findByDescription(String description){
        return tableViewRepository.findByDescription(description); }

    public void deleteTableById(Long id){
        tableViewRepository.deleteById(id);
    }
}
