ALTER TABLE "usuario" ADD COLUMN usuario_correo character varying(255) NULL;

ALTER TABLE "usuario" ADD COLUMN usuario_correo_verificado boolean NULL;

ALTER TABLE "usuario" ADD COLUMN usuario_creado_en time without time zone NULL;

ALTER TABLE "usuario" ADD COLUMN usuario_actualizado_en timestamp without time zone NULL;

ALTER TABLE "usuario" ALTER COLUMN fk_prof_id DROP NOT NULL;


ALTER TABLE "usuario" ADD CONSTRAINT uk_correo UNIQUE (usuario_correo);

ALTER TABLE "usuario" ADD CONSTRAINT uk_login UNIQUE (usuario_login);