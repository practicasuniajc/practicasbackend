CREATE TABLE "correo_verificacion"(
    correo_id serial NOT NULL,
    correo_token character varying(255) NOT NULL,
    correo_token_estado character varying(255),
    correo_creado_en timestamp without time zone NOT NULL,
    correo_actualizado_en timestamp without time zone NOT NULL,
    correo_caducidad_en timestamp without time zone NOT NULL,
    fk_usr_id serial NOT NULL,
    PRIMARY KEY (correo_id));

    ALTER TABLE "correo_verificacion" OWNER to "smilesoft";

CREATE TABLE "cambiar_contrasenia"(
    contrasenia_id serial NOT NULL,
    contrasenia_token character varying(255) NOT NULL,
    contrasenia_caducidad_en timestamp without time zone NOT NULL,
    fk_usr_id serial NOT NULL,
    PRIMARY KEY (contrasenia_id));

    ALTER TABLE "cambiar_contrasenia" OWNER to "smilesoft";


CREATE TABLE "dispositivo_usuario"(
    dispositivo_usuario_id serial NOT NULL,
    dispositivo_id character varying(255) NOT NULL,
    dispositivo_tipo character varying(255),
    actualizacion_activa boolean,
    dispositivo_creado_en timestamp without time zone NOT NULL,
    dispositivo_actualizado_en timestamp without time zone NOT NULL,
    fk_usr_id serial NOT NULL,
    PRIMARY KEY (dispositivo_usuario_id));

    ALTER TABLE "dispositivo_usuario" OWNER to "smilesoft";


CREATE TABLE "actualizar_token"(
    token_id serial NOT NULL,
    token_creado_en timestamp without time zone NOT NULL,
    token_actualizado_en timestamp without time zone NOT NULL,
    token_caducidad_en timestamp without time zone NOT NULL,
    token_recuento bigint NOT NULL,
    token character varying(255) NOT NULL,
    fk_usr_dispositivo_id serial NOT NULL,
    PRIMARY KEY (token_id));

    ALTER TABLE "actualizar_token" OWNER to "smilesoft";


ALTER TABLE "correo_verificacion" ADD CONSTRAINT fk_usuario FOREIGN KEY (fk_usr_id) REFERENCES "usuario" (usuario_id);
ALTER TABLE "correo_verificacion" ADD CONSTRAINT uk_token_correo UNIQUE (correo_token);
ALTER TABLE "cambiar_contrasenia" ADD CONSTRAINT fk_usuario FOREIGN KEY (fk_usr_id) REFERENCES "usuario" (usuario_id);
ALTER TABLE "cambiar_contrasenia" ADD CONSTRAINT uk_contrasenia_token UNIQUE (contrasenia_token);
ALTER TABLE "actualizar_token" ADD CONSTRAINT fk_dispositivo_usuario FOREIGN KEY (fk_usr_dispositivo_id) REFERENCES "dispositivo_usuario" (dispositivo_usuario_id);
ALTER TABLE "actualizar_token" ADD CONSTRAINT uk_token UNIQUE (token);
ALTER TABLE "dispositivo_usuario" ADD CONSTRAINT fk_usuario FOREIGN KEY (fk_usr_id) REFERENCES "usuario" (usuario_id);