 ALTER TABLE "examen_general" ALTER COLUMN exfgen_temp_oral TYPE integer,
  ALTER COLUMN exfgen_pulso TYPE integer,
  ALTER COLUMN exfgen_estatura TYPE integer,
  ALTER COLUMN exfgen_peso TYPE integer;

  ALTER TABLE "dettratamiento"
  ALTER COLUMN pt_valor TYPE integer;