 create table "pais" (
  pais_id   serial not null,
  pais_desc character varying(60) not null,
  primary key (pais_id));

  ALTER TABLE "pais" OWNER TO "smilesoft";


 create table "dpto_estado" (
  dpto_id    serial not null,
  dpto_desc  character varying(60),
  fk_pais_id serial not null,
  primary key (dpto_id));

  ALTER TABLE "dpto_estado" OWNER TO "smilesoft";

create table "ciudad" (
  ciudad_id   serial not null,
  ciudad_desc character varying(60) not null,
  fk_Dpto_id  serial not null,
  primary key (ciudad_id));

  ALTER TABLE "ciudad" OWNER TO "smilesoft";

create table "empresa" (
  emp_nit                  character varying(15) not null,
  emp_desc                 character varying(20),
  emp_sigla                character varying(15),
  emp_direccion            character varying(120),
  emp_telefono             character varying(10),
  emp_representante        character varying(120),
  emp_tel_representate     character varying(10),
  emp_correo_representante character varying(80),
  emp_correo               character varying(80),
  emp_activo               bool,
  fk_pais_id               serial not null,
  primary key (emp_nit));

  ALTER TABLE "empresa" OWNER TO "smilesoft";

create table "sucursal" (
  suc_nit                  character varying(15) not null,
  suc_desc                 character varying(20),
  suc_sigla                character varying(15),
  suc_direccion            character varying(120),
  suc_telefono             character varying(10),
  suc_telefono2            character varying(10),
  suc_representante        character varying(120),
  suc_tel_representate     character varying(10),
  suc_correo_representante character varying(80),
  suc_correo               character varying(80),
  suc_activo               bool,
  fk_emp_nit               character varying(15) not null,
  fk_ciudad_id             serial not null,
  primary key (suc_nit));

   ALTER TABLE "sucursal" OWNER TO "smilesoft";

create table "eps" (
  eps_id    serial not null,
  eps_desc  character varying(20),
  eps_sigla character varying(15),
  primary key (eps_id));

  ALTER TABLE "eps" OWNER TO "smilesoft";

create table "tipo_documento" (
  td_id    serial not null,
  td_desc  character varying(20),
  td_sigla character varying(6),
  primary key (td_id));

    ALTER TABLE "tipo_documento" OWNER TO "smilesoft";

 create table "paciente" (
  pac_dni              character varying(20) not null,
  pac_nombre           character varying(120),
  pac_genero           char(1),
  pac_ocupacion        character varying(120),
  pac_telefono         character varying(10),
  pac_lugar_nacimiento character varying(60),
  pac_fecha_nac        date,
  pac_edad             serial,
  pac_religion         character varying(60),
  pac_dir_residencia   character varying(60),
  pac_tel_residencia   character varying(10),
  pac_estadocivil      character varying(15),
  pac_escolaridad      character varying(20),
  pac_responsable      character varying(120),
  fk_eps_id            serial not null,
  fk_td_id             serial not null,
  primary key (pac_dni));

  ALTER TABLE "paciente" OWNER TO "smilesoft";

create table "profesional" (
  prof_id              serial not null,
  prof_nombre          character varying(120) not null,
  prof_fechanacimiento date,
  prof_dni             character varying(20) not null,
  prof_celular         character varying(10),
  prof_telef_casa      character varying(10),
  prof_telef_oficina   character varying(10),
  prof_correo          character varying(80),
  prof_dir_ofic        character varying(100),
  prof_dir_casa        character varying(100),
  fk_td_id             serial not null,
  primary key (prof_id));

  ALTER TABLE "profesional" OWNER TO "smilesoft";

 create table "rol" (
  rol_id   serial not null,
  rol_desc character varying(60) not null,
  primary key (rol_id));

  ALTER TABLE "rol" OWNER TO "smilesoft";

 create table "usuario" (
  usuario_id       serial not null,
  usuario_login    character varying(30) not null,
  usuario_password character varying(100) not null,
  usuario_activo   bool,
  fk_prof_id       serial not null,
  primary key (usuario_id));

  ALTER TABLE "usuario" OWNER TO "smilesoft";

create table "usuario_rol" (
  usrol_id      serial not null,
  fk_usuario_id serial not null unique,
  fk_rol_id     serial not null unique,
  primary key (usrol_id));

  ALTER TABLE "usuario_rol" OWNER TO "smilesoft";

 create table "historia_clinica" (
  hc_id      character varying(20) not null,
  fk_pac_dni character varying(20) not null,
  primary key (hc_id));

  ALTER TABLE "historia_clinica" OWNER TO "smilesoft";

create table "cie10" (
  cie_id    serial not null,
  cie_cie10 character varying(20) not null,
  cie_desc  character varying(120) not null,
  primary key (cie_id));

  ALTER TABLE "cie10" OWNER TO "smilesoft";

 create table "diagnostico_definitivo" (
  diagdef_id         serial not null,
  diagdef_pronostico character varying(255),
  diagdef_fecha      timestamp without time zone,
  fk_hc_id           character varying(20) not null,
  primary key (diagdef_id));

  ALTER TABLE "diagnostico_definitivo" OWNER TO "smilesoft";

create table "dagnostico_cie10" (
  diagcie_id                      serial not null,
  fk_diagdef_id serial not null,
  fk_cie10cie_id                     serial not null,
  primary key (diagcie_id));

  ALTER TABLE "dagnostico_cie10" OWNER TO "smilesoft";

create table "tratamiento" (
  t_id                 serial not null,
  fk_hc_id character varying(20) not null,
  primary key (t_id));

  ALTER TABLE "tratamiento" OWNER TO "smilesoft";

create table "evolucion" (
  evol_id          serial not null,
  evol_descripcion character varying(255),
  evol_fecha       timestamp without time zone,
  fk_t_id          serial not null,
  primary key (evol_id));

  ALTER TABLE "evolucion" OWNER TO "smilesoft";

create table "categoria_cups" (
  catcup_id              serial not null,
  catcup_desc            character varying(60),
  catcup_subcategoria_id serial not null,
  primary key (catcup_id));

  ALTER TABLE "categoria_cups" OWNER TO "smilesoft";

create table "cups" (
  cups_id                serial not null,
  cups_cup               character varying(20) not null,
  cups_desc              character varying(120) not null,
  fk_catcup_id serial not null,
  primary key (cups_id));

  ALTER TABLE "cups" OWNER TO "smilesoft";

create table "dettratamiento" (
  pt_id          serial not null,
  pt_cantidad    serial,
  pt_valor       float4,
  pt_no_sesiones serial,
  pt_aprobado    bool,
  pt_fecha       timestamp without time zone,
  fk_hc_id       character varying(20) not null,
  fk_cups_id     serial not null,
  fk_t_id        serial not null,
  primary key (pt_id));

  ALTER TABLE "dettratamiento" OWNER TO "smilesoft";

 create table "remision" (
  rem_id                serial not null,
  rem_motivo            character varying(255),
  fk_hc_id              character varying(20) not null,
  fk_remitente_prof_id  serial not null,
  fk_profesionalprof_id serial not null,
  primary key (rem_id));

  ALTER TABLE "remision" OWNER TO "smilesoft";

create table "anamnesis" (
  anam_id                  serial not null,
  anam_causa_consulta      character varying(255),
  anam_h_enfermedad_actual character varying(255),
  anam_amp                 character varying(255),
  anam_amf                 character varying(255),
  anam_tma                 character varying(255),
  anam_med_act             serial,
  anam_ant_odont           character varying(255),
  anam_habitos             character varying(400),
  anam_fecha               timestamp without time zone,
  fk_hc_id                 character varying(20) not null,
  primary key (anam_id));
comment on column "anamnesis"."anam_amp" is 'Antecedentes medicos personales';
comment on column "anamnesis"."anam_amf" is 'Antecendentes medicos familiares';
comment on column "anamnesis"."anam_tma" is 'Tratamiento médico actual';
comment on column "anamnesis"."anam_med_act" is 'medicación actual';
comment on column "anamnesis"."anam_ant_odont" is 'Antecedentes Odontológicos';

  ALTER TABLE "anamnesis" OWNER TO "smilesoft";


create table "auditoria" (
  aud_id          serial not null,
  aud_accion      character varying(255),
  aud_fecha       timestamp without time zone,
  aud_tabla       character varying(50),
  aud_descripcion character varying(255),
  fk_usuario_id   serial not null,
  primary key (aud_id));
comment on column "auditoria"."aud_accion" is 'insert, update, delete';
comment on column "auditoria"."aud_descripcion" is 'Solo se crea para update, delete';

  ALTER TABLE "auditoria" OWNER TO "smilesoft";

create table "diagnostico_presuntivo" (
  diagpre_id          serial not null,
  diagpre_diagnostico character varying(255),
  diagpre_fecha       timestamp without time zone,
  fk_hc_id            character varying(20) not null,
  primary key (diagpre_id));

    ALTER TABLE "diagnostico_presuntivo" OWNER TO "smilesoft";

create table "examen_extra_oral" (
  exoral_id         serial not null,
  exoral_musculos   character varying(255),
  exoral_atm        character varying(255),
  exoral_reg_subman character varying(255),
  exoral_cuello     character varying(255),
  exoral_nodos      character varying(255),
  exaoral_fecha     timestamp without time zone,
  fk_hc_id          character varying(20) not null,
  primary key (exoral_id));
comment on column "examen_extra_oral"."exoral_reg_subman" is 'Región Submandibular';

  ALTER TABLE "examen_extra_oral" OWNER TO "smilesoft";

create table "examen_general" (
  exfgen_id            serial not null,
  exfgen_pres_art_sis  serial,
  exfgen_pres_art_dias serial,
  exfgen_rh            character varying(10),
  exfgen_temp_oral     float4,
  exfgen_estatura      float4,
  exfgen_pulso         float4,
  exfgen_frec_resp     serial,
  exfgen_peso          float4,
  exfgen_est_conc      character varying(255),
  exfgen_fecha         timestamp without time zone not null,
  fk_hc_id             character varying(20) not null,
  primary key (exfgen_id));
comment on column "examen_general"."exfgen_pres_art_sis" is 'Presion Arterial Sistolica';
comment on column "examen_general"."exfgen_pres_art_dias" is 'Presión Arterial Diastolica';
comment on column "examen_general"."exfgen_frec_resp" is 'Frecuencia Respiratoria';
comment on column "examen_general"."exfgen_est_conc" is 'Estado de Conciencia';

  ALTER TABLE "examen_general" OWNER TO "smilesoft";


create table "examen_intra_oral" (
  exaintra_id             serial not null,
  exaintra_labios         character varying(255),
  exaintra_bermellon      character varying(255),
  exaintra_vest_bucal     character varying(255),
  exaintra_carrillos      character varying(255),
  exaintra_con_stenon     character varying(255),
  exaintra_pboca          character varying(255),
  exaintra_lengua         character varying(255),
  exaintra_amigdalas      character varying(255),
  exaintra_uvula          character varying(255),
  exaintra_orofaringe     character varying(255),
  exaintra_encia          character varying(255),
  exaintra_paladar_duro   character varying(255),
  exaintra_paladar_blando character varying(255),
  exaintra_frenillos      character varying(255),
  exaintra_cond_sublin    character varying(255),
  exaintra_fecha          timestamp without time zone,
  fk_hc_id                character varying(20) not null,
  primary key (exaintra_id));
comment on column "examen_intra_oral"."exaintra_vest_bucal" is 'Vestibulo Bucal';
comment on column "examen_intra_oral"."exaintra_con_stenon" is 'Conducto de Stenon';
comment on column "examen_intra_oral"."exaintra_pboca" is 'Piso de Boca';
comment on column "examen_intra_oral"."exaintra_cond_sublin" is 'Conductos Sublinguales';


  ALTER TABLE "examen_intra_oral" OWNER TO "smilesoft";


create table "indice_placa" (
  indplac_id serial not null,
  fk_hc_id      character varying(20) not null,
  primary key (indplac_id));

    ALTER TABLE "indice_placa" OWNER TO "smilesoft";

create table "odontograma" (
  odonto_id serial not null,
  fk_hc_id  character varying(20) not null,
  primary key (odonto_id));

    ALTER TABLE "odontograma" OWNER TO "smilesoft";

create table "revisionx_sistema" (
  rxs_id         serial not null,
  rxs_sis_car    char(1),
  rxs_sis_endo   char(1),
  rxs_sis_gas    char(1),
  rxs_sis_loco   char(1),
  rxs_sis_neu    char(1),
  rxs_sis_resp   char(1),
  rxs_sis_piel   char(1),
  rxs_sis_ge_uri char(1),
  rxs_desc       character varying(255),
  rxs_fecha      timestamp without time zone,
  fk_hc_id       character varying(20) not null,
  primary key (rxs_id));
comment on column "revisionx_sistema"."rxs_sis_car" is 'Sistema Cardiovascular';
comment on column "revisionx_sistema"."rxs_sis_endo" is 'Sistema Endocrino';
comment on column "revisionx_sistema"."rxs_sis_gas" is 'Sistema Gastrointestinal';
comment on column "revisionx_sistema"."rxs_sis_loco" is 'Sistema Locomotor';
comment on column "revisionx_sistema"."rxs_sis_neu" is 'Sistema Neurologico';
comment on column "revisionx_sistema"."rxs_sis_resp" is 'Sistema Respiratorio';
comment on column "revisionx_sistema"."rxs_sis_piel" is 'Piel y anexos (fáneras)';
comment on column "revisionx_sistema"."rxs_sis_ge_uri" is 'Sistema Genito Urinario';

  ALTER TABLE "revisionx_sistema" OWNER TO "smilesoft";

create table "rx" (
  rx_id          serial not null,
  rx_url         character varying(255),
  rx_diagnostico character varying(255),
  rx_fecha       timestamp without time zone,
  fk_hc_id       character varying(20) not null,
  primary key (rx_id));

    ALTER TABLE "rx" OWNER TO "smilesoft";


alter table "dpto_estado" add constraint FKDpto_Estad530240 foreign key (fk_pais_id) references "pais" (pais_id);
alter table "ciudad" add constraint FKciudad85692 foreign key (fk_Dpto_id) references "dpto_estado" (dpto_id);
alter table "sucursal" add constraint FKsucursal613586 foreign key (fk_emp_nit) references "empresa" (emp_nit);
alter table "empresa" add constraint FKempresa249825 foreign key (fk_pais_id) references "pais" (pais_id);
alter table "sucursal" add constraint FKsucursal84510 foreign key (fk_ciudad_id) references "ciudad" (ciudad_id);
alter table "paciente" add constraint FKpaciente223014 foreign key (fk_eps_id) references "eps" (eps_id);
alter table "profesional" add constraint FKProfesiona291850 foreign key (fk_td_id) references "tipo_documento" (td_id);
alter table "paciente" add constraint FKpaciente336782 foreign key (fk_td_id) references "tipo_documento" (td_id);
alter table "usuario" add constraint FKusuario996094 foreign key (fk_prof_id) references "profesional" (prof_id);
alter table "usuario_rol" add constraint FKusuario_Ro886330 foreign key (fk_usuario_id) references "usuario" (usuario_id);
alter table "usuario_rol" add constraint FKusuario_Ro628115 foreign key (fk_rol_id) references "rol" (rol_id);
alter table "historia_clinica" add constraint FKHistoriaCl577277 foreign key (fk_pac_dni) references "paciente" (pac_dni);
alter table "rx" add constraint FKrx373217 foreign key (fk_hc_id) references "historia_clinica" (hc_id);
alter table "examen_extra_oral" add constraint FKExamenExtr773764 foreign key (fk_hc_id) references "historia_clinica" (hc_id);
alter table "examen_general" add constraint FKExamenGene209535 foreign key (fk_hc_id) references "historia_clinica" (hc_id);
alter table "examen_intra_oral" add constraint FKExamenIntr890078 foreign key (fk_hc_id) references "historia_clinica" (hc_id);
alter table "anamnesis" add constraint FKanamnesis391409 foreign key (fk_hc_id) references "historia_clinica" (hc_id);
alter table "revisionx_sistema" add constraint FKRevisionxS133447 foreign key (fk_hc_id) references "historia_clinica" (hc_id);
alter table "diagnostico_presuntivo" add constraint FKDiagnostic185215 foreign key (fk_hc_id) references "historia_clinica" (hc_id);
alter table "diagnostico_definitivo" add constraint FKDiagnostic231119 foreign key (fk_hc_id) references "historia_clinica" (hc_id);
alter table "tratamiento" add constraint FKTratamient875798 foreign key (fk_hc_id) references "historia_clinica" (hc_id);
alter table "odontograma" add constraint FKOdontogram804401 foreign key (fk_hc_id) references "historia_clinica" (hc_id);
alter table "indice_placa" add constraint FKIndicePlac936016 foreign key (fk_hc_id) references "historia_clinica" (hc_id);
alter table "dagnostico_cie10" add constraint FKDiagnostic628488 foreign key (fk_diagdef_id) references "diagnostico_definitivo" (diagdef_id);
alter table "dagnostico_cie10" add constraint FKDiagnostic906760 foreign key (fk_cie10cie_id) references "cie10"(cie_id);
alter table "categoria_cups" add constraint FKCategoriaC954256 foreign key (catcup_subcategoria_id) references "categoria_cups" (catcup_id);
alter table "cups" add constraint FKCUPS44990 foreign key (fk_catcup_id) references "categoria_cups" (catcup_id);
alter table "dettratamiento" add constraint FKDetTratami116274 foreign key (fk_cups_id) references "cups" (cups_id);
alter table "dettratamiento" add constraint FKDetTratami119662 foreign key (fk_t_id) references "tratamiento" (t_id);
alter table "evolucion" add constraint FKevolucion406884 foreign key (fk_t_id) references "tratamiento" (t_id);
alter table "remision" add constraint FKremision180745 foreign key (fk_hc_id) references "historia_clinica" (hc_id);
alter table "remision" add constraint FKremision564354 foreign key (fk_remitente_prof_id) references "profesional" (prof_id);
alter table "remision" add constraint FKremision142637 foreign key (fk_profesionalprof_id) references "profesional" (prof_id);
alter table "auditoria" add constraint FKauditoria139486 foreign key (fk_usuario_id) references "usuario" (usuario_id);
