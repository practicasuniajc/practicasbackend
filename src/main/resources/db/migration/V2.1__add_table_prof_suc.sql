CREATE TABLE "profesional_sucursal"(
    prof_suc_id serial NOT NULL,
    fk_prof_id serial NOT NULL,
    fk_nit_suc character varying(15) NOT NULL,
    PRIMARY KEY (prof_suc_id)
);


ALTER TABLE "profesional_sucursal" ADD CONSTRAINT fk_sucursal FOREIGN KEY (fk_nit_suc) REFERENCES "sucursal" (suc_nit);
ALTER TABLE "profesional_sucursal" ADD CONSTRAINT fk_profesional FOREIGN KEY (fk_prof_id) REFERENCES "profesional" (prof_id);