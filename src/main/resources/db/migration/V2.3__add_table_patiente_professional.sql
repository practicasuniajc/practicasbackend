CREATE TABLE "paciente_profesional"(
    pp_id serial NOT NULL,
    fk_paciente_id character varying(20) NOT NULL,
    fk_profesional_id serial NOT NULL,
    PRIMARY KEY (pp_id)
);

ALTER TABLE "paciente_profesional" OWNER to smilesoft;


ALTER TABLE "paciente_profesional" ADD CONSTRAINT fk_paciente FOREIGN KEY (fk_paciente_id) REFERENCES "paciente" (pac_dni);
ALTER TABLE "paciente_profesional" ADD CONSTRAINT fk_profesional FOREIGN KEY (fk_profesional_id) REFERENCES "profesional" (prof_id);