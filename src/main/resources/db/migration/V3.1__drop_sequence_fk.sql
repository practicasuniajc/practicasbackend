drop sequence "dpto_estado_fk_pais_id_seq" cascade;

drop sequence "ciudad_fk_dpto_id_seq" cascade;

drop sequence "empresa_fk_pais_id_seq" cascade;

drop sequence "paciente_fk_eps_id_seq" cascade;

drop sequence "paciente_fk_td_id_seq" cascade;

drop sequence "profesional_fk_td_id_seq" cascade;

drop sequence "usuario_fk_prof_id_seq" cascade;

drop sequence "usuario_rol_fk_usuario_id_seq" cascade;

drop sequence "usuario_rol_fk_rol_id_seq" cascade;

drop sequence "dagnostico_cie10_fk_diagdef_id_seq" cascade;

drop sequence "dagnostico_cie10_fk_cie10cie_id_seq" cascade;

drop sequence "evolucion_fk_t_id_seq" cascade;

drop sequence "cups_fk_catcup_id_seq" cascade;

drop sequence "dettratamiento_fk_cups_id_seq" cascade;

drop sequence "dettratamiento_fk_t_id_seq" cascade;

drop sequence "remision_fk_remitente_prof_id_seq" cascade;

drop sequence "remision_fk_profesionalprof_id_seq" cascade;

drop sequence "auditoria_fk_usuario_id_seq" cascade;

drop sequence "correo_verificacion_fk_usr_id_seq" cascade;

drop sequence "cambiar_contrasenia_fk_usr_id_seq" cascade;

drop sequence "dispositivo_usuario_fk_usr_id_seq" cascade;

drop sequence "actualizar_token_fk_usr_dispositivo_id_seq" cascade;

drop sequence "profesional_sucursal_fk_prof_id_seq" cascade;

drop sequence "sucursal_fk_ciudad_id_seq" cascade;

drop sequence "paciente_profesional_fk_profesional_id_seq" cascade;