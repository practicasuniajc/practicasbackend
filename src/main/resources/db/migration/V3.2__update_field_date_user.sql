ALTER TABLE "usuario" DROP COLUMN usuario_creado_en;

ALTER TABLE "usuario" ADD COLUMN usuario_creado_en timestamp with time zone;