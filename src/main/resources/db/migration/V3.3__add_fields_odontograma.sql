ALTER TABLE "odontograma" ADD COLUMN fk_prof_id serial NOT NULL;

ALTER TABLE "odontograma" ADD COLUMN odonto_diente integer NOT NULL;

ALTER TABLE "odontograma" ADD COLUMN odonto_procedimiento character varying(250);

ALTER TABLE "odontograma" ADD COLUMN odonto_afeccion character varying(250);

ALTER TABLE "odontograma" ADD COLUMN odonto_cara character varying(250);

ALTER TABLE "odontograma" ADD COLUMN odonto_fecha timestamp without time zone NOT NULL;

ALTER TABLE "odontograma" ADD COLUMN odonto_parcial boolean NOT NULL;


ALTER TABLE "odontograma" ADD CONSTRAINT fk_profesional FOREIGN KEY (fk_prof_id) REFERENCES "profesional" (prof_id);

drop sequence "odontograma_fk_prof_id_seq" cascade;