ALTER TABLE "tratamiento" ADD COLUMN t_fecha timestamp without time zone NULL;

ALTER TABLE "dettratamiento" ADD COLUMN fk_cie10_id serial NOT NULL;

ALTER TABLE "profesional" ALTER COLUMN prof_nombre TYPE character varying (120) COLLATE pg_catalog."default";
ALTER TABLE "profesional" ALTER COLUMN prof_nombre DROP NOT NULL;
ALTER TABLE "profesional" ALTER COLUMN prof_dni TYPE character varying (20) COLLATE pg_catalog."default";
ALTER TABLE "profesional" ALTER COLUMN prof_dni DROP NOT NULL;
ALTER TABLE "profesional" ALTER COLUMN fk_td_id DROP NOT NULL;


ALTER TABLE "dettratamiento" ADD CONSTRAINT fk_cie10 FOREIGN KEY (fk_cie10_id) REFERENCES "cie10" (cie_id);