ALTER TABLE "examen_general" DROP COLUMN exfgen_peso;
ALTER TABLE "examen_general" DROP COLUMN exfgen_temp_oral;
ALTER TABLE "examen_general" ADD COLUMN exfgen_peso numeric(8, 2);
ALTER TABLE "examen_general" ADD COLUMN exfgen_temp_oral numeric(8, 2);