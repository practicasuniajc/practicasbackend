ALTER TABLE "odontograma" ADD COLUMN odonto_final boolean NULL;
ALTER TABLE "cie10"  ALTER COLUMN cie_desc TYPE varchar(500);
ALTER TABLE "cups"  ALTER COLUMN cups_desc TYPE varchar(500);
ALTER TABLE "categoria_cups"  ALTER COLUMN catcup_desc TYPE varchar(500);