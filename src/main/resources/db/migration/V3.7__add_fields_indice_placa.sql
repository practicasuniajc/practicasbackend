ALTER TABLE "indice_placa" ADD COLUMN indplac_diente integer NOT NULL;

ALTER TABLE "indice_placa" ADD COLUMN indplac_arriba boolean;

ALTER TABLE "indice_placa" ADD COLUMN indplac_izquierda boolean; 

ALTER TABLE "indice_placa" ADD COLUMN indplac_derecha boolean;

ALTER TABLE "indice_placa" ADD COLUMN indplac_abajo boolean; 

ALTER TABLE "indice_placa" ADD COLUMN indplac_centro boolean; 

ALTER TABLE "indice_placa" ADD COLUMN indplac_no_diente boolean; 

ALTER TABLE "indice_placa" ADD COLUMN fk_pf_id serial NOT NULL; 

ALTER TABLE "indice_placa" ADD COLUMN indplac_fecha timestamp without time zone NOT NULL;