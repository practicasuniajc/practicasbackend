ALTER TABLE "examen_extra_oral" DROP COLUMN exoral_musculos;
ALTER TABLE "examen_extra_oral" DROP COLUMN exoral_reg_subman;
ALTER TABLE "examen_extra_oral" DROP COLUMN exoral_cuello;
ALTER TABLE "examen_extra_oral" DROP COLUMN exoral_nodos;
ALTER TABLE "examen_extra_oral" ADD COLUMN exoral_res character varying(255);


ALTER TABLE "examen_intra_oral" DROP COLUMN exaintra_labios;
ALTER TABLE "examen_intra_oral" DROP COLUMN exaintra_bermellon;
ALTER TABLE "examen_intra_oral" DROP COLUMN exaintra_vest_bucal;
ALTER TABLE "examen_intra_oral" DROP COLUMN exaintra_carrillos;
ALTER TABLE "examen_intra_oral" DROP COLUMN exaintra_con_stenon;
ALTER TABLE "examen_intra_oral" DROP COLUMN exaintra_pboca;
ALTER TABLE "examen_intra_oral" DROP COLUMN exaintra_lengua;
ALTER TABLE "examen_intra_oral" DROP COLUMN exaintra_amigdalas;
ALTER TABLE "examen_intra_oral" DROP COLUMN exaintra_uvula;
ALTER TABLE "examen_intra_oral" DROP COLUMN exaintra_orofaringe;
ALTER TABLE "examen_intra_oral" DROP COLUMN exaintra_encia;
ALTER TABLE "examen_intra_oral" DROP COLUMN exaintra_paladar_duro;
ALTER TABLE "examen_intra_oral" DROP COLUMN exaintra_paladar_blando;
ALTER TABLE "examen_intra_oral" DROP COLUMN exaintra_frenillos;
ALTER TABLE "examen_intra_oral" DROP COLUMN exaintra_cond_sublin;
ALTER TABLE "examen_intra_oral" ADD COLUMN exaintra_res character varying(255);