ALTER TABLE "evolucion" DROP CONSTRAINT fkevolucion406884;
ALTER TABLE "evolucion" DROP COLUMN fk_t_id;
ALTER TABLE "evolucion" ADD COLUMN fk_det_tratamiento_id serial NOT NULL;
ALTER TABLE "evolucion" ADD CONSTRAINT fk_det_tratamiento FOREIGN KEY (fk_det_tratamiento_id) REFERENCES "dettratamiento" (pt_id);

ALTER TABLE "dettratamiento" DROP COLUMN pt_aprobado;
ALTER TABLE "dettratamiento" DROP COLUMN pt_cantidad;
ALTER TABLE "dettratamiento" DROP COLUMN pt_no_sesiones;
ALTER TABLE "dettratamiento" ALTER COLUMN pt_valor TYPE numeric (18, 2);
ALTER TABLE "dettratamiento" ALTER COLUMN pt_valor SET NOT NULL;
ALTER TABLE "dettratamiento" ALTER COLUMN pt_fecha SET NOT NULL;
ALTER TABLE "dettratamiento" ADD COLUMN pt_dientes character varying(500) NOT NULL;
ALTER TABLE "dettratamiento" ADD COLUMN pt_estado_realizado boolean;
ALTER TABLE "dettratamiento" ADD COLUMN pt_estado_pagado boolean;

ALTER TABLE "tratamiento" ADD COLUMN t_estado boolean;
ALTER TABLE "tratamiento" ADD COLUMN fk_cons_id serial NOT NULL;
ALTER TABLE "tratamiento" ADD CONSTRAINT fk_consulta FOREIGN KEY (fk_cons_id) REFERENCES "consulta" (cons_id);