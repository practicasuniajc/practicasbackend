ALTER TABLE "evolucion" DROP CONSTRAINT fk_det_tratamiento;
ALTER TABLE "evolucion" DROP COLUMN fk_det_tratamiento_id;

ALTER TABLE "anamnesis" RENAME fk_cons_id TO fk_detalle_hc_id;
ALTER TABLE "anamnesis" ADD CONSTRAINT "fk_detalle_historia" FOREIGN KEY (fk_detalle_hc_id) REFERENCES "detalle_historia" (cons_id);

ALTER TABLE "diagnostico_definitivo" RENAME fk_cons_id TO fk_detalle_hc_id;
ALTER TABLE "diagnostico_definitivo" ADD CONSTRAINT fk_detalle_historia FOREIGN KEY (fk_detalle_hc_id) REFERENCES "detalle_historia" (cons_id);

ALTER TABLE "diagnostico_presuntivo" RENAME fk_cons_id TO fk_detalle_hc_id;
ALTER TABLE "diagnostico_presuntivo" ADD CONSTRAINT fk_detalle_historia FOREIGN KEY (fk_detalle_hc_id) REFERENCES "detalle_historia" (cons_id);

ALTER TABLE "examen_general" RENAME fk_cons_id TO fk_detalle_hc_id;
ALTER TABLE "examen_general" ADD CONSTRAINT fk_detalle_historia FOREIGN KEY (fk_detalle_hc_id) REFERENCES "detalle_historia" (cons_id);

ALTER TABLE "examen_extra_oral" RENAME fk_cons_id TO fk_detalle_hc_id;
ALTER TABLE "examen_extra_oral" ADD CONSTRAINT fk_detalle_historia FOREIGN KEY (fk_detalle_hc_id) REFERENCES "detalle_historia" (cons_id);

ALTER TABLE "examen_intra_oral" RENAME fk_cons_id TO fk_detalle_hc_id;
ALTER TABLE "examen_intra_oral" ADD CONSTRAINT fk_detalle_historia FOREIGN KEY (fk_detalle_hc_id) REFERENCES "detalle_historia" (cons_id);

ALTER TABLE "indice_placa" RENAME fk_cons_id TO fk_detalle_hc_id;
ALTER TABLE "indice_placa" ADD CONSTRAINT fk_detalle_historia FOREIGN KEY (fk_detalle_hc_id) REFERENCES "detalle_historia" (cons_id);

ALTER TABLE "odontograma" RENAME fk_cons_id TO fk_detalle_hc_id;
ALTER TABLE "odontograma" ADD CONSTRAINT fk_detalle_historia FOREIGN KEY (fk_detalle_hc_id) REFERENCES "detalle_historia" (cons_id);

ALTER TABLE "remision" RENAME fk_cons_id TO fk_detalle_hc_id;
ALTER TABLE "remision" ADD CONSTRAINT fk_detalle_historia FOREIGN KEY (fk_detalle_hc_id) REFERENCES "detalle_historia" (cons_id);

ALTER TABLE "rx" RENAME fk_cons_id TO fk_detalle_hc_id;
ALTER TABLE "rx" ADD CONSTRAINT fk_detalle_historia FOREIGN KEY (fk_detalle_hc_id) REFERENCES "detalle_historia" (cons_id);





