CREATE TABLE "evolucion_detalle"(
    ed_id serial NOT NULL,
    fk_evol_id serial NOT NULL,
    fk_det_tratamiento_id serial NOT NULL,
    PRIMARY KEY (ed_id)
);

ALTER TABLE "evolucion_detalle" OWNER to smilesoft;

ALTER TABLE "evolucion_detalle" ADD CONSTRAINT fk_evolucion FOREIGN KEY (fk_evol_id) REFERENCES "evolucion" (evol_id);
ALTER TABLE "evolucion_detalle" ADD CONSTRAINT fk_detalle_tratamiento FOREIGN KEY (fk_det_tratamiento_id) REFERENCES "dettratamiento" (pt_id);