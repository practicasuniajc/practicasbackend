DROP TABLE IF EXISTS "detalle_historia" CASCADE;

CREATE TABLE "detalle_historia"(
    cons_id serial NOT NULL ,
    cons_fecha timestamp without time zone NOT NULL,
    fk_prof_id serial NOT NULL,
    fk_hc_id character varying(20) NOT NULL,
    cons_estado "char" NOT NULL,
    CONSTRAINT detalle_historia_pkey PRIMARY KEY (cons_id)
);

ALTER TABLE "detalle_historia" OWNER to smilesoft;

ALTER TABLE "detalle_historia" ADD CONSTRAINT fk_historia_clinica FOREIGN KEY (fk_hc_id) REFERENCES "historia_clinica" (hc_id);
ALTER TABLE "detalle_historia" ADD CONSTRAINT fk_profesional FOREIGN KEY (fk_prof_id) REFERENCES "profesional" (prof_id);