ALTER TABLE "tratamiento" RENAME fk_cons_id TO fk_detalle_hc_id;
ALTER TABLE "tratamiento" ADD CONSTRAINT "fk_detalle_historia" FOREIGN KEY (fk_detalle_hc_id) REFERENCES "detalle_historia" (cons_id);


ALTER TABLE "anamnesis" ADD CONSTRAINT "fk_detalle_historia" FOREIGN KEY (fk_detalle_hc_id) REFERENCES "detalle_historia" (cons_id);
ALTER TABLE "diagnostico_definitivo" ADD CONSTRAINT fk_detalle_historia FOREIGN KEY (fk_detalle_hc_id) REFERENCES "detalle_historia" (cons_id);
ALTER TABLE "diagnostico_presuntivo" ADD CONSTRAINT fk_detalle_historia FOREIGN KEY (fk_detalle_hc_id) REFERENCES "detalle_historia" (cons_id);
ALTER TABLE "examen_general" ADD CONSTRAINT fk_detalle_historia FOREIGN KEY (fk_detalle_hc_id) REFERENCES "detalle_historia" (cons_id);
ALTER TABLE "examen_extra_oral" ADD CONSTRAINT fk_detalle_historia FOREIGN KEY (fk_detalle_hc_id) REFERENCES "detalle_historia" (cons_id);
ALTER TABLE "examen_intra_oral" ADD CONSTRAINT fk_detalle_historia FOREIGN KEY (fk_detalle_hc_id) REFERENCES "detalle_historia" (cons_id);
ALTER TABLE "indice_placa" ADD CONSTRAINT fk_detalle_historia FOREIGN KEY (fk_detalle_hc_id) REFERENCES "detalle_historia" (cons_id);
ALTER TABLE "odontograma" ADD CONSTRAINT fk_detalle_historia FOREIGN KEY (fk_detalle_hc_id) REFERENCES "detalle_historia" (cons_id);
ALTER TABLE "remision" ADD CONSTRAINT fk_detalle_historia FOREIGN KEY (fk_detalle_hc_id) REFERENCES "detalle_historia" (cons_id);
ALTER TABLE "rx" ADD CONSTRAINT fk_detalle_historia FOREIGN KEY (fk_detalle_hc_id) REFERENCES "detalle_historia" (cons_id);