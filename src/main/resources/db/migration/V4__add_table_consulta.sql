CREATE TABLE "consulta"(
    cons_id serial NOT NULL,
    cons_fecha timestamp without time zone NOT NULL,
    fk_prof_id serial NOT NULL,
    fk_hc_id character varying(20) NOT NULL,
    cons_estado "char" NOT NULL,
    PRIMARY KEY (cons_id)
);

ALTER TABLE "consulta" OWNER to smilesoft;


ALTER TABLE "consulta" ADD CONSTRAINT fk_profesional FOREIGN KEY (fk_prof_id) REFERENCES "profesional" (prof_id);
ALTER TABLE "consulta" ADD CONSTRAINT fk_historia_clinica FOREIGN KEY (fk_hc_id) REFERENCES "historia_clinica" (hc_id);