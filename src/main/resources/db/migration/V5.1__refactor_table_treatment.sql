ALTER TABLE "tratamiento" DROP COLUMN t_estado;
ALTER TABLE "tratamiento" ADD COLUMN t_valor_total numeric(18, 2);
ALTER TABLE "tratamiento" ADD COLUMN t_estado character varying(100);