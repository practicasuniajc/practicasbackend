CREATE TABLE "plan_costos"(
    pc_id serial NOT NULL,
    pc_abono numeric(18, 2) NOT NULL,
    pc_fecha_abono timestamp without time zone NOT NULL,
    pc_saldo numeric(18, 2) NOT NULL,
    fk_detalle_tratamiento_id serial NOT NULL,
    PRIMARY KEY (pc_id)
);

ALTER TABLE "plan_costos" OWNER to smilesoft;

ALTER TABLE "plan_costos" ADD CONSTRAINT fk_detalle_tratamiento FOREIGN KEY (fk_detalle_tratamiento_id) REFERENCES "dettratamiento" (pt_id);