ALTER TABLE "plan_costos" ALTER COLUMN pc_saldo TYPE numeric (18, 2);
ALTER TABLE "plan_costos" ALTER COLUMN pc_saldo DROP NOT NULL;

ALTER TABLE "plan_costos" ALTER COLUMN pc_fecha_abono DROP NOT NULL;

ALTER TABLE "plan_costos" ALTER COLUMN pc_abono TYPE numeric (18, 2);
ALTER TABLE "plan_costos" ALTER COLUMN pc_abono DROP NOT NULL;