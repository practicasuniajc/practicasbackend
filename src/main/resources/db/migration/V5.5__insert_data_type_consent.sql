INSERT INTO "tipo_consentimiento"(cs_id, cs_descripcion, cs_activo)
    VALUES (1, 'Consentimiento de carta de aceptación', true);

INSERT INTO "tipo_consentimiento"(cs_id, cs_descripcion, cs_activo)
    VALUES (2, 'Consentimiento de tratamiento de datos e imágenes paciente', true);

INSERT INTO "tipo_consentimiento"(cs_id, cs_descripcion, cs_activo)
    VALUES (3, 'Consentimiento de profilaxis dental', true);

INSERT INTO "tipo_consentimiento"(cs_id, cs_descripcion, cs_activo)
    VALUES (4, 'Consentimiento de blanqueamiento', true);

INSERT INTO "tipo_consentimiento"(cs_id, cs_descripcion, cs_activo)
    VALUES (5, 'Consentimiento de craniomandibular', true);

INSERT INTO "tipo_consentimiento"(cs_id, cs_descripcion, cs_activo)
    VALUES (6, 'Consentimiento de endodoncia', true);

INSERT INTO "tipo_consentimiento"(cs_id, cs_descripcion, cs_activo)
    VALUES (7, 'Consentimiento informado', true);

INSERT INTO "tipo_consentimiento"(cs_id, cs_descripcion, cs_activo)
    VALUES (8, 'Consentimiento de odontología general', true);

INSERT INTO "tipo_consentimiento"(cs_id, cs_descripcion, cs_activo)
    VALUES (9, 'Consentimiento de anestesia local', true);

INSERT INTO "tipo_consentimiento"(cs_id, cs_descripcion, cs_activo)
    VALUES (10, 'Consentimiento de cirugía de cordales', true);

INSERT INTO "tipo_consentimiento"(cs_id, cs_descripcion, cs_activo)
    VALUES (11, 'Consentimiento de cirugía oral', true);

INSERT INTO "tipo_consentimiento"(cs_id, cs_descripcion, cs_activo)
    VALUES (12, 'Consentimiento de operatoria y estética', true);

INSERT INTO "tipo_consentimiento"(cs_id, cs_descripcion, cs_activo)
    VALUES (13, 'Consentimiento de ortodoncia', true);

INSERT INTO "tipo_consentimiento"(cs_id, cs_descripcion, cs_activo)
    VALUES (14, 'Consentimiento de exodoncia simple', true);

INSERT INTO "tipo_consentimiento"(cs_id, cs_descripcion, cs_activo)
    VALUES (15, 'Consentimiento de odontopediatría', true);

INSERT INTO "tipo_consentimiento"(cs_id, cs_descripcion, cs_activo)
    VALUES (16, 'Consentimiento para rehabilitación oral de prótesis dental', true);

INSERT INTO "tipo_consentimiento"(cs_id, cs_descripcion, cs_activo)
    VALUES (17, 'Consentimiento de odontología general operatoria', true);

INSERT INTO "tipo_consentimiento"(cs_id, cs_descripcion, cs_activo)
    VALUES (18, 'Consentimiento de periodoncia', true);