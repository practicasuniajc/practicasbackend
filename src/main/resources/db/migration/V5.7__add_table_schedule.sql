CREATE TABLE "agendamiento"(
    ag_id serial NOT NULL,
    fk_cups_id serial NOT NULL,
    ag_nombre_paciente character varying(100) NOT NULL,
    ag_fecha timestamp without time zone NOT NULL,
    fk_detalle_tratamiento_id serial NOT NULL,
    ag_estado character varying(20),
    PRIMARY KEY (ag_id)
);

ALTER TABLE "agendamiento" OWNER to smilesoft;

ALTER TABLE "agendamiento" ADD CONSTRAINT fk_cups FOREIGN KEY (fk_cups_id) REFERENCES "cups" (cups_id);
ALTER TABLE "agendamiento" ADD CONSTRAINT fk_detalle_tratamiento FOREIGN KEY (fk_detalle_tratamiento_id) REFERENCES "dettratamiento" (pt_id);