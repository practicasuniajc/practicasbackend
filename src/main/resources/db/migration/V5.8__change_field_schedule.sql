ALTER TABLE "agendamiento" ALTER COLUMN fk_detalle_tratamiento_id DROP NOT NULL;
ALTER TABLE "agendamiento" ADD COLUMN fk_profesional_id serial;

ALTER TABLE "agendamiento" ADD CONSTRAINT fk_proesional FOREIGN KEY (fk_profesional_id) REFERENCES "profesional" (prof_id);