CREATE TABLE "tipo_consentimiento"(
    cs_id serial NOT NULL,
    cs_descripcion character varying(500) NOT NULL,
    cs_activo boolean NOT NULL,
    PRIMARY KEY (cs_id)
);

ALTER TABLE "tipo_consentimiento" OWNER to smilesoft;