CREATE TABLE "tabla"(
    tb_id serial NOT NULL,
    tb_descripcion character varying(100) NOT NULL,
    tb_fecha timestamp without time zone NOT NULL,
    tb_valor numeric(18, 9) NOT NULL,
    tb_estado boolean NOT NULL,
    PRIMARY KEY (tb_id)
);
