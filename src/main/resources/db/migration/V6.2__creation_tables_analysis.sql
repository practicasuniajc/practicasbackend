CREATE TABLE "ana_dina_oclusion" (
    ado_id serial NOT NULL,
    fk_detalle_hc_id serial NOT NULL,
    ado_deflexion boolean,
    ado_orient_deflex character varying(12),
    ado_magnit_deflex double precision,
    ado_contact_deflect character varying(500),
    ado_excur_protusiva character varying(500),
    ado_fchristensen character varying(500),
    ado_excur_latder character varying(500),
    ado_protecan_der character varying(500),
    ado_cont_notrabajo_der character varying(500),
    ado_excur_latizq character varying(500),
    ado_protecan_izq character varying(500),
    ado_cont_notrabajo_izq character varying(500),
    ado_observaciones character varying(500),
    ado_fecha date
);

    ALTER TABLE "ana_dina_oclusion" OWNER TO "smilesoft";



CREATE TABLE "ana_est_modelosi" (
    ami_id serial NOT NULL,
    fk_analisisest_id integer,
    ami_articulador integer,
    ami_marca character varying(255),
    ami_fecha_analisis date,
    ami_distan_intercondilar integer,
    ami_punto_anterior character varying(255),
    ami_gui_incisal character varying(255),
    ami_dimen_vertical character varying(255),
    ami_model_feurilizado character varying(255),
    ami_reg_interdentales character varying(500),
    ami_aspneg_proc character varying(255)
);

    ALTER TABLE "ana_est_modelosi" OWNER TO "smilesoft";



CREATE TABLE "ana_est_modelosii" (
    aemii_id serial NOT NULL,
    fk_analisisest_id serial,
    aemii_forma character varying(255),
    aemii_posicion character varying(255),
    aemii_deflexion boolean,
    aemii_orient_deflex character varying(12),
    aemii_magnit_deflex double precision,
    aemii_contact_deflect character varying(500),
    aemii_excur_protusiva character varying(500),
    aemii_fchristensen character varying(500),
    aemii_excur_latder character varying(500),
    aemii_protecan_der character varying(500),
    aemii_cont_notrabajo_der character varying(500),
    aemii_excur_latizq character varying(500),
    aemii_protecan_izq character varying(500),
    aemii_cont_notrabajo_izq character varying(500),
    aemii_observaciones character varying(500),
    aemii_date date
);

    ALTER TABLE "ana_est_modelosii" OWNER TO "smilesoft";



CREATE TABLE "analisis_estatico" (
    ae_id serial NOT NULL,
    fk_detalle_hc_id serial,
    ae_fecha date
);

    ALTER TABLE "analisis_estatico" OWNER TO "smilesoft";



CREATE TABLE "arquitectura_gingival" (
    ag_papila_espinterd boolean,
    ag_papila_achatada boolean,
    ag_papila_aplanada boolean,
    ag_crateriforme boolean,
    ag_papila_otra character varying(100),
    ag_marg_filo boolean,
    ag_marg_redondo boolean,
    ag_marg_retraido boolean,
    ag_marg_otra character varying(100),
    ag_marg_desc character varying(255),
    ag_encia_adherida boolean,
    ag_encia_distancia character varying(50),
    ag_encia_desc character varying(255),
    ag_biotipo_period boolean,
    ag_biotipo_desc character varying(255),
    ag_id serial NOT NULL,
    fk_periodon_id serial
);

    ALTER TABLE "arquitectura_gingival" OWNER TO "smilesoft";



CREATE TABLE "buffer_salivar" (
    bs_php character varying(255),
    bs_phinm character varying(255),
    bs_ph5 character varying(255),
    bs_ph10 character varying(255),
    bs_ph15 character varying(255),
    bs_ph20 character varying(255),
    bs_id serial NOT NULL
);

    ALTER TABLE "buffer_salivar" OWNER TO "smilesoft";



CREATE TABLE "caract_examen_clinico" (
    exc_sang_localizado boolean,
    exc_sang_generalizado boolean,
    exc_sang_espontaneo boolean,
    exc_sang_estimulo boolean,
    exc_sang_desc character varying(255),
    exc_exudado boolean,
    exc_exudado_desc character varying(255),
    exc_movilidad boolean,
    exc_movilidad_desc character varying(255),
    exc_movilidad_grado character varying(255),
    exc_lesion_furca integer,
    exc_furca_desc character varying(255),
    exc_diente_bolsa boolean,
    exc_dientebosa_desc character varying(255),
    exc_calculo boolean,
    exc_tipo_calculo character varying(50),
    exc_calculo_desc character varying(255),
    exc_id serial NOT NULL,
    fk_periodon_id serial
);

    ALTER TABLE "caract_examen_clinico" OWNER TO "smilesoft";



CREATE TABLE "descrip_gingival" (
    dg_color character varying(255),
    dg_desc_color character varying(255),
    dg_consistencia character varying(255),
    dg_desc_consistenc character varying(255),
    dg_aspecto character varying(255),
    dg_desc_aspecto character varying(255),
    dg_id serial NOT NULL,
    fk_periodon_id serial
);

    ALTER TABLE "descrip_gingival" OWNER TO "smilesoft";



CREATE TABLE "det_ana_estatico" (
    dane_arcada_sup character varying(500),
    dane_arcada_inf character varying(500),
    dane_lineamedia character varying(500),
    dane_sobremor_vert character varying(500),
    dane_sobremor_horiz character varying(500),
    dane_mord_ant character varying(500),
    dane_mord_post character varying(500),
    dane_encaje_ant character varying(500),
    dane_post_der character varying(500),
    dane_post_izq character varying(500),
    dane_id serial   NOT NULL,
    fk_analisisest_id serial,
    dane_date date
);

    ALTER TABLE "det_ana_estatico" OWNER TO "smilesoft";



CREATE TABLE "factor_riesgo" (
    fdr_enfer_sistemic integer,
    fdr_caries integer,
    fdr_conten_dieta integer,
    fdr_frecuencia_dieta integer,
    fdr_cantplaca_bact integer,
    fdr_expos_alfluor integer,
    fdr_habitos_higiene integer,
    fdr_id serial NOT NULL,
    fk_detalle_hc_id serial,
    fdr_fecha date,
    fdr_phs double precision,
    fk_buffersaliv_id serial
);

    ALTER TABLE "factor_riesgo" OWNER TO "smilesoft";



CREATE TABLE "periodoncia" (
    per_id serial NOT NULL,
    per_fecha date,
    fk_detalle_hc_id serial
);

    ALTER TABLE "periodoncia" OWNER TO "smilesoft";



CREATE TABLE "prog_articulador" (
    pa_tipo character varying(255),
    pa_guiacondp_d double precision,
    pa_guiacondp_i double precision,
    pa_movprogp_d double precision,
    pa_movprogp_i double precision,
    pa_angbennetp_d double precision,
    pa_angbennetp_i double precision,
    pa_guiancondl_d double precision,
    pa_guiancondl_i double precision,
    pa_movprogl_d double precision,
    pa_movprogl_i double precision,
    pa_angbennetl_d double precision,
    pa_angbennetl_i double precision,
    pa_id serial NOT NULL,
    fk_analisisest_id serial,
    pa_date date
);

    ALTER TABLE "prog_articulador" OWNER TO "smilesoft";



CREATE TABLE "rx_estruc_anatomica" (
    rxea_fecha_toma date,
    rxea_orbita boolean,
    rxea_tabique boolean,
    rxea_espina boolean,
    rxea_paladarduro boolean,
    rxea_senomaxi boolean,
    rxea_malar boolean,
    rxea_tuberosidad boolean,
    rxea_eminartic boolean,
    rxea_fglenoidea boolean,
    rxea_condilo boolean,
    rxea_esigmoidea boolean,
    rxea_apofcoro boolean,
    rxea_ramandibula boolean,
    rxea_angmandib boolean,
    rxea_cuerpmandib boolean,
    rxea_rinfmandib boolean,
    rxea_imageneologicos character varying(255),
    fk_detalle_hc_id serial,
    rxea_id serial NOT NULL,
    fk_rxpanoramica_id serial
);

    ALTER TABLE "rx_estruc_anatomica" OWNER TO "smilesoft";



CREATE TABLE "rx_panoramica" (
    rxpa_fecha_toma date,
    rxpa_maxsup_basal boolean,
    rxpa_maxsup_alveolar boolean,
    rxpa_maxinf_basal boolean,
    rxpa_maxinf_alveolar boolean,
    rxpa_maxi_observ character varying(255),
    rxpa_tipo_dentic character varying(50),
    rxpa_cant_dientes integer,
    rxpa_dientes_erupc integer,
    rxpa_dientes_sinerup integer,
    rxpa_cant_dient_c1 integer,
    rxpa_cant_dient_c2 integer,
    rxpa_cant_dient_c3 integer,
    rxpa_cant_dient_c4 integer,
    rxpa_anomalia_dent boolean,
    rxpa_anom_tam character varying(255),
    rxpa_anom_num integer,
    rxpa_anom_forma character varying(255),
    rxpa_anom_posic character varying(255),
    rxpa_anom_estruct character varying(255),
    rxpa_anom_observ character varying(255),
    fk_detalle_hc_id serial,
    rxpa_id serial NOT NULL
);

    ALTER TABLE "rx_panoramica" OWNER TO "smilesoft";



CREATE TABLE "rx_periapical" (
    rxper_fecha date,
    rxper_diente integer,
    rxper_cortical character varying(255),
    rxper_cresta_osea character varying(255),
    rxper_corona character varying(255),
    rxper_cam_conductpul character varying(255),
    rxper_furca character varying(255),
    rxper_raices character varying(255),
    rxper_zona_apical character varying(255),
    rxper_perdiosea_tipo character varying(255),
    rxper_perdiosea_grado character varying(255),
    rxper_rel_coronaraiz character varying(255),
    rxper_espacio_ligaperiod character varying(255),
    rxper_operador integer,
    fk_detalle_hc_id serial,
    rxper_id serial NOT NULL
);

    ALTER TABLE "rx_periapical" OWNER TO "smilesoft";



CREATE TABLE "tipo_factor_riesgo" (
    tfr_tipofactor integer,
    tfr_valor integer,
    tfr_descripcion character varying(255),
    tfr_id serial NOT NULL
);

    ALTER TABLE "tipo_factor_riesgo" OWNER TO "smilesoft";



ALTER TABLE ONLY "ana_dina_oclusion" ADD CONSTRAINT ana_dina_oclusion_pkey PRIMARY KEY (ado_id);
ALTER TABLE ONLY "ana_est_modelosi" ADD CONSTRAINT ana_est_modelosi_pkey PRIMARY KEY (ami_id);
ALTER TABLE ONLY "ana_est_modelosii" ADD CONSTRAINT ana_est_modelosii_pkey PRIMARY KEY (aemii_id);
ALTER TABLE ONLY "analisis_estatico" ADD CONSTRAINT analisis_estatico_pkey PRIMARY KEY (ae_id);
ALTER TABLE ONLY "arquitectura_gingival" ADD CONSTRAINT arquitectura_gingival_pkey PRIMARY KEY (ag_id);
ALTER TABLE ONLY "buffer_salivar" ADD CONSTRAINT buffer_salivar_pkey PRIMARY KEY (bs_id);
ALTER TABLE ONLY "caract_examen_clinico" ADD CONSTRAINT caract_examen_clinico_pkey PRIMARY KEY (exc_id);
ALTER TABLE ONLY "descrip_gingival" ADD CONSTRAINT descrip_gingival_pkey PRIMARY KEY (dg_id);
ALTER TABLE ONLY "det_ana_estatico" ADD CONSTRAINT det_ana_estatico_pkey PRIMARY KEY (dane_id);
ALTER TABLE ONLY "factor_riesgo" ADD CONSTRAINT factor_riesgo_pkey PRIMARY KEY (fdr_id);
ALTER TABLE ONLY "periodoncia" ADD CONSTRAINT periodoncia_pkey PRIMARY KEY (per_id);
ALTER TABLE ONLY "prog_articulador" ADD CONSTRAINT prog_articulador_pkey PRIMARY KEY (pa_id);
ALTER TABLE ONLY "rx_estruc_anatomica" ADD CONSTRAINT rx_estruc_anatomica_pkey PRIMARY KEY (rxea_id);
ALTER TABLE ONLY "rx_panoramica" ADD CONSTRAINT rx_panoramica_pkey PRIMARY KEY (rxpa_id);
ALTER TABLE ONLY "rx_periapical" ADD CONSTRAINT rx_periapical_pkey PRIMARY KEY (rxper_id);
ALTER TABLE ONLY "tipo_factor_riesgo" ADD CONSTRAINT tipo_factor_riesgo_pkey PRIMARY KEY (tfr_id);



ALTER TABLE ONLY "ana_est_modelosi" ADD CONSTRAINT ana_est_modelosi_fk_analisisest_id_fkey FOREIGN KEY (fk_analisisest_id) REFERENCES "analisis_estatico" (ae_id);
ALTER TABLE ONLY "ana_est_modelosii" ADD CONSTRAINT ana_est_modelosii_fk_analisisest_id_fkey FOREIGN KEY (fk_analisisest_id) REFERENCES "analisis_estatico" (ae_id);
ALTER TABLE ONLY "arquitectura_gingival" ADD CONSTRAINT arquitectura_gingival_fk_periodon_id_fkey FOREIGN KEY (fk_periodon_id) REFERENCES "periodoncia" (per_id);
ALTER TABLE ONLY "caract_examen_clinico" ADD CONSTRAINT caract_examen_clinico_fk_periodon_id_fkey FOREIGN KEY (fk_periodon_id) REFERENCES "periodoncia" (per_id);
ALTER TABLE ONLY "descrip_gingival" ADD CONSTRAINT descrip_gingival_fk_periodon_id_fkey FOREIGN KEY (fk_periodon_id) REFERENCES "periodoncia" (per_id);
ALTER TABLE ONLY "det_ana_estatico" ADD CONSTRAINT det_ana_estatico_fk_analisisest_id_fkey FOREIGN KEY (fk_analisisest_id) REFERENCES "analisis_estatico" (ae_id);
ALTER TABLE ONLY "factor_riesgo" ADD CONSTRAINT factor_riesgo_fdr_cantplaca_bact_fkey FOREIGN KEY (fdr_cantplaca_bact) REFERENCES "tipo_factor_riesgo" (tfr_id);
ALTER TABLE ONLY "factor_riesgo" ADD CONSTRAINT factor_riesgo_fdr_caries_fkey FOREIGN KEY (fdr_caries) REFERENCES "tipo_factor_riesgo" (tfr_id);
ALTER TABLE ONLY "factor_riesgo" ADD CONSTRAINT factor_riesgo_fdr_conten_dieta_fkey FOREIGN KEY (fdr_conten_dieta) REFERENCES "tipo_factor_riesgo" (tfr_id);
ALTER TABLE ONLY "factor_riesgo" ADD CONSTRAINT factor_riesgo_fdr_enfer_sistemic_fkey FOREIGN KEY (fdr_enfer_sistemic) REFERENCES "tipo_factor_riesgo" (tfr_id);
ALTER TABLE ONLY "factor_riesgo" ADD CONSTRAINT factor_riesgo_fdr_expos_alfluor_fkey FOREIGN KEY (fdr_expos_alfluor) REFERENCES "tipo_factor_riesgo" (tfr_id);
ALTER TABLE ONLY "factor_riesgo" ADD CONSTRAINT factor_riesgo_fdr_frecuencia_dieta_fkey FOREIGN KEY (fdr_frecuencia_dieta) REFERENCES "tipo_factor_riesgo" (tfr_id);
ALTER TABLE ONLY "factor_riesgo" ADD CONSTRAINT factor_riesgo_fdr_habitos_higiene_fkey FOREIGN KEY (fdr_habitos_higiene) REFERENCES "tipo_factor_riesgo" (tfr_id);
ALTER TABLE ONLY "factor_riesgo" ADD CONSTRAINT factor_riesgo_fk_buffersaliv_id_fkey FOREIGN KEY (fk_buffersaliv_id) REFERENCES "buffer_salivar" (bs_id);
ALTER TABLE ONLY "ana_dina_oclusion" ADD CONSTRAINT fk_detalle_hc FOREIGN KEY (fk_detalle_hc_id) REFERENCES "detalle_historia" (cons_id);
ALTER TABLE ONLY "factor_riesgo" ADD CONSTRAINT fk_detallechc_fdr FOREIGN KEY (fk_detalle_hc_id) REFERENCES "detalle_historia" (cons_id);
ALTER TABLE ONLY "analisis_estatico" ADD CONSTRAINT fk_hc_detalle_ae FOREIGN KEY (fk_detalle_hc_id) REFERENCES "detalle_historia" (cons_id);
ALTER TABLE ONLY "periodoncia" ADD CONSTRAINT periodoncia_fk_detalle_hc_id_fkey FOREIGN KEY (fk_detalle_hc_id) REFERENCES "detalle_historia" (cons_id);
ALTER TABLE ONLY "prog_articulador" ADD CONSTRAINT prog_articulador_fk_analisisest_id_fkey FOREIGN KEY (fk_analisisest_id) REFERENCES "analisis_estatico" (ae_id);
ALTER TABLE ONLY "rx_estruc_anatomica" ADD CONSTRAINT rx_estruc_anatomica_fk_rxpanoramica_id_fkey FOREIGN KEY (fk_rxpanoramica_id) REFERENCES "rx_panoramica" (rxpa_id);
ALTER TABLE ONLY "rx_panoramica" ADD CONSTRAINT rx_panoramica_fk_detalle_hc_id_fkey FOREIGN KEY (fk_detalle_hc_id) REFERENCES "detalle_historia" (cons_id);
ALTER TABLE ONLY "rx_periapical" ADD CONSTRAINT rx_periapical_fk_detalle_hc_id_fkey FOREIGN KEY (fk_detalle_hc_id) REFERENCES "detalle_historia" (cons_id);